<div class="modal center-modal" id="modal-add-product" tabindex="-1" style="display: block; padding-right: 15px;"
     aria-modal="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" style="overflow-y: scroll;height: 800px;">
            <div class="modal-header">
                <h5 class="modal-title">Add Product</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="form-add-product">
                @csrf
                <div class="modal-body">
                    <div class="message-error mb-20 bg-danger p-10" style="display: none">
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <h5>Product Name<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Product Price<span class="text-danger">*</span></h5>
                                <div class="group">
                                    <input type="number" name="price" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Sale (%)<span class="text-danger">*</span></h5>
                                <div class="group">
                                    <input type="number" name="sale" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Description</h5>
                                <div class="group">
                                    <input type="text" name="description" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Category<span class="text-danger">*</span></h5>
                                <select class="control select2" style="width: 100%;" name="category_id" id="category_id">
                                    <option value="" >Select Category</option>
                                    @foreach($listCategory as $category)
                                        <option value="{{$category->id}}">{{str_repeat('--',$category->level).$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <h5>Image</h5>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="main-image" name="avatar">
                                <label class="custom-file-label" for="main-image">Choose file</label>
                            </div>
                            <div class="show-image p-3">
                                <img src="https://ipsumimage.appspot.com/300x280" alt="main-image">
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <h5>Tags</h5>
                                <select class="control choose_tags" style="width: 50%;" name="tags[]" multiple id="tags">
                                </select>
                            </div>
                            <div class="form-group">
                                <h5>Quantity<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="number" name="quantity" class="form-control">
                                </div>
                            </div>
                            <div class="form-group validate">
                                <h5>Status <span class="text-danger">*</span></h5>
                                <fieldset class="controls">
                                    <input name="active" type="radio" id="active" value="1" checked>
                                    <label for="active">Active</label>
                                </fieldset>
                                <fieldset>
                                    <input name="active" type="radio" id="inactive" value="0">
                                    <label for="inactive">InActive</label>
                                </fieldset>
                            </div>
                            <div class="form-group">
                                <h5>Content</h5>
                                <textarea class="form-control my-editor" rows="18" cols="12"
                                          name="content" id="getData"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 5px;padding-top: 16px;">
                        <div class="col-4">
                            <h5>Image Detail</h5>
                            <div class="custom-file" >
                                <input type="file" multiple class="custom-file-input" name="image_detail[]" id="image-detail">
                                <label class="custom-file-label" for="image-detail">Choose file</label>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="list-image-detail"
                                 style="width: 100%; height: 270px; border: 2px solid greenyellow; border-radius: 5px; padding-left: 15px">
                                <div class="row">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-uniform" style="width: 100%;">
                    <button type="button" class="btn btn-rounded btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-rounded btn-primary float-right" id="btn-save-product"
                            data-action="{{route('products.store')}}">Save changes
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
