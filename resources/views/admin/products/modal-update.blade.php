<div class="modal center-modal" id="modal-update-product" tabindex="-1" style="display: block; padding-right: 15px;"
     aria-modal="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" style="overflow-y: scroll;height: 800px;">
            <div class="modal-header">
                <h5 class="modal-title">Update Product</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="form-update-product">
                @csrf
                <div class="modal-body">
                    <div class="message-error mb-20 bg-danger p-10" style="display: none">
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <h5>Product Name<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="name" class="form-control" id="name">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Product Price<span class="text-danger">*</span></h5>
                                <div class="group">
                                    <input type="number" name="price" class="form-control" id="price">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Sale (%)<span class="text-danger">*</span></h5>
                                <div class="group">
                                    <input type="number" name="sale" class="form-control" id="sale">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Description</h5>
                                <div class="group">
                                    <input type="text" name="description" class="form-control" id="description">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Category<span class="text-danger">*</span></h5>
                                <select class="control select2" style="width: 100%;" name="category_id" id="category_update_id">
                                    <option value="" >Select Category</option>
                                </select>
                            </div>
                            <h5>Image</h5>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="main-image-update" name="avatar">
                                <label class="custom-file-label" for="main-image-update">Choose file</label>
                            </div>
                            <div class="show-image p-3 show-image-update">
                                <img src="" alt="main-image">
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <h5>Tags</h5>
                                <select class="control choose_tags" style="width: 50%;" name="tags[]" multiple id="tags_update">
                                </select>
                            </div>
                            <div class="form-group">
                                <h5>Quantity<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="number" name="quantity" class="form-control" id="quantity">
                                </div>
                            </div>
                            <div class="form-group validate">
                                <h5>Status <span class="text-danger">*</span></h5>
                                <fieldset class="controls">
                                    <input name="active" type="radio" id="active_update" value="1">
                                    <label for="active_update">Active</label>
                                </fieldset>
                                <fieldset>
                                    <input name="active" type="radio" id="inactive_update" value="0">
                                    <label for="inactive_update">InActive</label>
                                </fieldset>
                            </div>
                            <div class="form-group">
                                <h5>Content</h5>
                                <textarea class="form-control my-editor" rows="18" cols="12"
                                          name="content" id="getDataUpdate"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-uniform" style="width: 100%;">
                    <button type="button" class="btn btn-rounded btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-rounded btn-primary float-right" id="btn-save-product-update">Save changes
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>
