<div class="box-body no-padding" id="list-product" data-action="{{route('products.list')}}">
    <div class="table-responsive">
        <table class="table table-hover text-center" >
            <tbody>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>avatar</th>
                <th>value</th>
                <th>content</th>
                <th>description</th>
                <th>other</th>
                <th>active</th>
                <th>action</th>
            </tr>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td style="width: 200px;"><img src="{{ $product->avatar }}" alt="image"></td>
                    <td>
                        <ul style="list-style:circle; text-align: left">
                            <li>Price: {{ number_format($product->price,2,",",".") }} VND</li>
                            <li>Sale: {{ $product->sale}} %</li>
                        </ul>
                    </td>
                    <td style="width: 400px;"><div style="height: 200px; overflow-y: scroll">{!! $product->content !!}</div></td>
                    <td style="width: 400px;"><div style="height: 200px; overflow-y: scroll">{!! $product->description !!}</div></td>
                    <td>
                        <ul style="list-style:circle; text-align: left">
                            <li>Category: {{ $product->categories->name }}</li>
                            <li>Quantity: {{ $product->quantity }}</li>
                            <li>View: {{ $product->view }} </li>
                            <li>Pay: {{ $product->pay }} </li>
                            <li>View: {{ $product->view }} </li>
                        </ul>
                    </td>
                    <td style="width: 110px">
                        <span class="badge badge-pill {{$product->active==1?'badge-success':'badge-danger'}}">
                            {{$product->active==1?'Active':'Inactive'}}
                        </span>
                        @can('edit-product')
                            <a class="change-status" href="#"
                               data-action="{{route('products.change-status',$product->id)}}">
                                <i class="fa fa-fw fa-refresh"></i>
                            </a>
                        @endcan
                    </td>
                    <td style="width: 110px">
                        @can('edit-product')
                            <a href="#" class="text-dark mr-15 update-product" style="font-size: 30px"
                               data-action="{{route('products.update',$product->id)}}"
                               data-url="{{route('products.edit',$product->id)}}" >
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>
                        @endcan
                        @can('delete-product')
                            <a href="#" class="text-secondary delete-product" style="font-size: 30px"
                               data-action="{{ route('products.destroy', $product->id) }}">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $products->links() !!}
    </div>
</div>
