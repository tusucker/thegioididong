@extends('layouts.master')

@section('title')
    <title>Admin - Products</title>
@endsection

@section('css')

@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">List Products</h4>
                        @can('add-product')
                            <button type="button" class="btn btn-success ml-15 open-modal-add-product">Add Product
                            </button>
                        @endcan
                        <div class="box-controls pull-right">
                            <div class="lookup lookup-circle lookup-right">
                                <form id="form-search">
                                    <label>
                                        <input type="text" name="search" id="search">
                                    </label>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding" id="list-product" data-action="{{route('products.list')}}">
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

    @include('admin.products.modal-add')
    @include('admin.products.modal-update')
@endsection

@section('js')

    <script src="js/admin/product.js"></script>
@endsection



