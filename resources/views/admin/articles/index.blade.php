@extends('layouts.master')

@section('title')
    <title>Admin - Articles</title>
@endsection

@section('css')

@endsection

@section('content')
    <section class="content">
        @include('components.message-success')
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">List Articles</h4>
                        @can('add-article')
                            <a href="{{ route('articles.create') }}" class="btn btn-success ml-15">Add Article
                            </a>
                        @endcan
                        <div class="box-controls pull-right">
                            <div class="lookup lookup-circle lookup-right">
                                <form id="form-search">
                                    <label>
                                        <input type="text" name="search" id="search">
                                    </label>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <tbody>
                                <tr>
                                    <th>id</th>
                                    <th>name</th>
                                    <th>description</th>
                                    <th>content</th>
                                    <th>author</th>
                                    <th>image</th>
                                    <th>view</th>
                                    <th>created at</th>
                                    <th>action</th>
                                </tr>
                                @foreach($articles as $article)
                                    <tr>
                                        <td>{{ $article->id }}</td>
                                        <td>{{ $article->name }}</td>
                                        <td>
                                            <div style="width: 300px; height: 300px; overflow-y: scroll">
                                                {!! $article->description !!}
                                            </div>

                                        </td>
                                        <td>
                                            <div style="width: 500px; height: 300px; overflow-y: scroll">{!! $article->content !!}</div>
                                        </td>
                                        <td>{{ $article->user['name'] }}</td>
                                        <td style="width: 200px;"><img src="{{ $article->avatar }}" alt="image"></td>
                                        <td>{{ $article->view }}</td>
                                        <td>{{ $article->created_at }}</td>
                                        <td style="width: 110px">
                                            @can('edit-article')
                                                <a href="{{ route('articles.edit', $article->id) }}" class="text-dark mr-15" style="font-size: 30px" >
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                            @can('delete-article')
                                                <a href="{{ route('articles.destroy', $article->id) }}" class="text-secondary delete-article" style="font-size: 30px" >
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="js/admin/article.js"></script>
@endsection



