@extends('layouts.master')

@section('title')
    <title>Admin - Articles</title>
@endsection

@section('css')

@endsection

@section('content')
    <section class="content">
        @include('components.message-error')
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Edit article</h4>
                    </div>
                    <form action="{{ route('articles.update', $article->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Article name</label>
                                    <input type="text" class="form-control" placeholder="Enter article name here"
                                           name="name" value="{{ old('name') ?? $article->name }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <label>Article image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="main-image" name="avatar">
                                    <label class="custom-file-label" for="main-image">Choose file</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="show-image p-3" style="max-height: 300px; overflow: hidden">
                                    <img src="{{ $article->avatar }}" alt="main-image">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Article description</label>
                                        <textarea class="form-control my-editor" rows="18" cols="12"
                                                  name="description" >{{ old('description') ?? $article->description }}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Article content</label>
                                        <textarea class="form-control my-editor" rows="18" cols="12"
                                                  name="content" >{{ old('content') ?? $article->content }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
{{--                            <div class="row m-0">--}}
{{--                                <div class="col-6">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Meta title</label>--}}
{{--                                        <input type="text" class="form-control" placeholder="Enter meta title here"--}}
{{--                                               name="seo_title" value="{{ old('seo_title') ?? $article->seo_title }}">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-6">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Meta description</label>--}}
{{--                                        <input type="text" class="form-control" placeholder="Enter meta description here"--}}
{{--                                               name="seo_description" value="{{ old('seo_description') ?? $article->seo_description }}">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="row d-flex justify-content-end pr-15 mt-15">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('js')
    <script src="js/admin/article.js"></script>
@endsection



