<div class="modal center-modal" id="modal-detail" tabindex="-1" style="display: block; padding-right: 15px;"
     aria-modal="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Invoice Detail</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <section class="invoice printableArea">
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-md-6 invoice-col">
                            <strong>From</strong>
                            <address>
                                <strong class="text-blue font-size-24">{{ $order->name }}</strong><br>
                                <strong class="d-inline">{{ $order->address }}</strong><br>
                                <strong>Phone: {{ $order->phone }} &nbsp;&nbsp;&nbsp;&nbsp; Email: {{ $order->email }}</strong>
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-12 invoice-col mb-15">
                            <div class="invoice-details row no-margin">
                                <div class="col-md-6 col-lg-4"><b>Order ID:</b> {{ $order->id }}</div>
                                <div class="col-md-6 col-lg-4"><b>Payment Due:</b> {{ $order->created_at }}</div>
                                <div class="col-md-6 col-lg-4"><b>Account:</b> {{ $order->user_id == 0 ? 'Khách' : 'Thành viên' }}</div>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>#</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th class="text-right">Quantity</th>
                                    <th class="text-right">Unit Cost</th>
                                    <th class="text-right">Subtotal</th>
                                </tr>
                                @foreach($order->orderDetail as $key => $detail)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $detail->product->name }}</td>
                                        <td>
                                            <img style="width: 50px; height: 50px" src="{{ $detail->product->avatar }}" alt="">
                                        </td>
                                        <td class="text-right">{{ $detail->quantity }}</td>
                                        <td class="text-right">{{ number_format($detail->price - $detail->price * $detail->sale / 100, 2, ',', '.') }} VND</td>
                                        <td class="text-right">{{ number_format(($detail->price - $detail->price * $detail->sale / 100) * $detail->quantity, 2, ',', '.') }} VND</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-12 text-right">
                            <p class="lead"><b>Payment Due</b><span class="text-danger"> {{ $order->created_at }} </span></p>
                            <div class="total-payment">
                                <h3><b>Total :</b> {{ number_format($order->total_money, 2, ',', '.') }} VND</h3>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
            </div>
        </div>
    </div>
</div>
