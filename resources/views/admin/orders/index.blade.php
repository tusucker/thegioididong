@extends('layouts.master')

@section('title')
    <title>Admin - Orders</title>
@endsection

@section('css')

@endsection

@section('content')
    <section class="content">
        @include('components.message-success')
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">List Orders</h4>

                        <div class="box-controls pull-right">
                            <div class="lookup lookup-circle lookup-right">
                                <form id="form-search">
                                    <label>
                                        <input type="text" name="search" id="search">
                                    </label>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <tbody>
                                <tr>
                                    <th>id</th>
                                    <th>Info</th>
                                    <th>Money</th>
                                    <th>Account</th>
                                    <th>Status</th>
                                    <th>Time</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($listOrder as $order)
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td style="text-align: left; font-size: 16px">
                                            <ul style="list-style:circle">
                                                <li>Name: {{ $order->name }}</li>
                                                <li>Email: {{ $order->email }}</li>
                                                <li>Phone: {{ $order->phone }}</li>
                                                <li>Address: {{ $order->address }}</li>
                                            </ul>
                                        </td>
                                        <td>{{ number_format($order->total_money,2,",",".") }} VND</td>
                                        <td>
                                            @if($order->user_id != 0)
                                                <span class="badge badge-success">Thành viên</span>
                                            @else
                                                <span class="badge badge-dark">Khách</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($order->status == 1)
                                                <span class="badge badge-success">Tiếp nhận</span>
                                            @elseif($order->status == 2)
                                                <span class="badge badge-dark">Đang vận chuyển</span>
                                            @elseif($order->status == 3)
                                                <span class="badge badge-info">Đã bàn giao</span>
                                            @else
                                                <span class="badge badge-danger">Đã hủy</span>
                                            @endif
                                        </td>
                                        <td>{{ $order->created_at }}</td>
                                        <td style="width: 200px; text-align: left">
                                            <ul>
                                                <li>
                                                    <a href="{{ route('orders.detail', ['id' => $order->id]) }}" class="show-detail">
                                                       Xem chi tiết
                                                    </a>
                                                </li>
                                                <li><a href="{{ route('orders.status-to-transport', ['id' => $order->id]) }}">Đang vận chuyển</a></li>
                                                <li><a href="{{ route('orders.status-to-done', ['id' => $order->id]) }}">Đã bàn giao</a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $listOrder->links() !!}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.orders.modal-detail')
@endsection

@section('js')
    <script src="js/admin/order.js"></script>
@endsection



