@extends('layouts.master')

@section('title')
    <title>Admin - Roles</title>
@endsection

@section('css')

@endsection

@section('content')
    <section class="content">
        @include('components.message-success')
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">List Roles</h4>
                        @can('add-role')
                            <a href="{{ route('roles.create') }}" class="btn btn-success ml-15 open-modal-add-product">Add Role
                            </a>
                        @endcan
                        <div class="box-controls pull-right">
                            <div class="lookup lookup-circle lookup-right">
                                <form id="form-search">
                                    <label>
                                        <input type="text" name="search" id="search">
                                    </label>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <tbody>
                                <tr>
                                    <th>id</th>
                                    <th>name</th>
                                    <th>display name</th>
                                    <th>action</th>
                                </tr>
                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $role->id }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>{!! $role->display_name !!}</td>
                                        <td style="width: 110px">
                                            @can('edit-role')
                                                <a href="{{ route('roles.edit', $role->id) }}" class="text-dark mr-15" style="font-size: 30px" >
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                            @can('delete-role')
                                                <a href="{{ route('roles.destroy', $role->id) }}" class="text-secondary delete-role" style="font-size: 30px" >
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="js/admin/role.js"></script>
@endsection



