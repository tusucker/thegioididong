@extends('layouts.master')

@section('title')
    <title>Admin - Roles</title>
@endsection

@section('css')

@endsection

@section('content')
    <section class="content">
        @include('components.message-error')
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Edit role</h4>
                    </div>
                    <form action="{{ route('roles.update', $role->id) }}" method="POST" >
                        @csrf
                        <div class="box-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Role name</label>
                                        <input type="text" class="form-control" placeholder="Enter role name here"
                                               name="name" value="{{ old('name') ?? $role->name }}">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Display name</label>
                                        <input type="text" class="form-control" placeholder="Enter display name here"
                                               name="display_name" value="{{ old('display_name') ?? $role->display_name }}">
                                    </div>
                                </div>
                            </div>
                            @foreach($listPermissionParent as $permissionParent)
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card border-info mb-3">
                                            <div class="card-header">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="{{ $permissionParent->id }}" class="checkbox-parent">
                                                    <label for="{{ $permissionParent->id }}">Module {{ $permissionParent->name }}</label>
                                                </div>
                                            </div>
                                            <div class="card-body text-info">
                                                <div class="row">
                                                    @foreach($permissionParent->permissionChildren as $permission)
                                                        <div class="col-3">
                                                            <input type="checkbox"
                                                                   id="{{ $permission->id }}"
                                                                   class="checkbox-children"
                                                                   name="permission_id[]"
                                                                   value="{{ $permission->id }}"
                                                                   {{ $lisPermissionChecked->contains('id', $permission->id) ? 'checked' : '' }}
                                                            >
                                                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="row d-flex justify-content-end pr-15 mt-15">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('js')
    <script src="js/admin/role.js"></script>
@endsection



