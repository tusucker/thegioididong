@extends('layouts.master')

@section('title')
    <title>Admin - Users</title>
@endsection

@section('css')

@endsection

@section('content')
    <section class="content">
        @include('components.message-success')
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">List Users</h4>
                        @can('add-user')
                            <a href="{{ route('users.create') }}" class="btn btn-success ml-15 open-modal-add-product">Add User
                            </a>
                        @endcan
                        <div class="box-controls pull-right">
                            <div class="lookup lookup-circle lookup-right">
                                <form id="form-search">
                                    <label>
                                        <input type="text" name="search" id="search">
                                    </label>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <tbody>
                                <tr>
                                    <th>id</th>
                                    <th>name</th>
                                    <th>email</th>
                                    <th>role</th>
                                    <th>action</th>
                                </tr>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{!! $user->email !!}</td>
                                        <td style="width: 400px">
                                            @foreach($user->roles as $role)
                                                <span style="padding: 5px; margin: 2px; background: red; color: aliceblue">{{ $role->name }}</span>
                                            @endforeach
                                        </td>
                                        <td style="width: 110px">
                                            @can('edit-user')
                                                <a href="{{ route('users.edit', $user->id) }}" class="text-dark mr-15" style="font-size: 30px" >
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                            @can('delete-user')
                                                <a href="{{ route('users.destroy', $user->id) }}" class="text-secondary delete-user" style="font-size: 30px" >
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="js/admin/user.js"></script>
@endsection



