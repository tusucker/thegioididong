@extends('layouts.master')

@section('title')
    <title>Admin - Users</title>
@endsection

@section('css')

@endsection

@section('content')
    <section class="content">
        @include('components.message-error')
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Edit user</h4>
                    </div>
                    <form action="{{ route('users.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>User name</label>
                                        <input type="text" class="form-control" placeholder="Enter user name here"
                                               name="name" value="{{ old('name') ?? $user->name }}">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" placeholder="Enter your email here"
                                               name="email" value="{{ old('email') ?? $user->email }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" placeholder="Enter new password here"
                                               name="password" value="{{ old('password') }}">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Role</label>
                                        <select class="form-control select2" multiple="multiple" name="role[]">
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}"
                                                        {{ in_array($role->id, $listRoleUserId) ? 'selected' : '' }}>
                                                    {{ $role->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label>Avatar</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="main-image" name="avatar">
                                        <label class="custom-file-label" for="main-image">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="show-image p-3" style="max-height: 300px; overflow: hidden">
                                    <img src="{{ $user->avatar }}" alt="main-image">
                                </div>
                            </div>
                            <div class="row d-flex justify-content-end pr-15 mt-15">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('js')
    <script src="js/admin/user.js"></script>
@endsection



