<div class="modal center-modal" id="modal-add-category" tabindex="-1" style="display: block; padding-right: 15px;" aria-modal="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form-add-category">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="message-error mb-20" style="display: none">
                        <span class="badge badge-danger"></span>
                    </div>
                    <div class="form-group">
                        <h5>Category Name<span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group list-parent" data-action="{{route('categories.list-parent')}}">
                    </div>
                    <div class="form-group validate">
                        <h5>Status <span class="text-danger">*</span></h5>
                        <fieldset class="controls">
                            <input name="active" type="radio" id="active" value="1" checked>
                            <label for="active">Active</label>
                        </fieldset>
                        <fieldset>
                            <input name="active" type="radio" id="inactive" value="0" >
                            <label for="inactive">InActive</label>
                        </fieldset>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image-1" name="banner[]">
                                <label class="custom-file-label" for="image-1">Choose file</label>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="visible-image visible-image-1">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-4">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image-2" name="banner[]">
                                <label class="custom-file-label" for="image-2">Choose file</label>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="visible-image visible-image-2">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-4">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image-3" name="banner[]">
                                <label class="custom-file-label" for="image-3">Choose file</label>
                            </div>
                        </div>
                        <div class="col-8 mt-3">
                            <div class="visible-image visible-image-3">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-4">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image-4" name="banner[]">
                                <label class="custom-file-label" for="image-4">Choose file</label>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="visible-image visible-image-4">
                                <div></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer modal-footer-uniform" style="width: 100%;">
                    <button type="button" class="btn btn-rounded btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-rounded btn-primary float-right" id="btn-save-category" data-action="{{route('categories.store')}}">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
