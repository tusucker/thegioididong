<div class="myadmin-dd" data-action="{{route('categories.tree-view')}}">
    <ol class="dd-list">
        @foreach($listCategoryParent as $category)
            @if($category->level==0)
                <li class="dd-item" >
                    <div class="dd-handle"> {{$category->name}} </div>
                    @if($category->categoryChildren->count())
                        <ol class="dd-list">
                            @foreach($category->categoryChildren as $categoryChild)
                                <li class="dd-item" data-id="3">
                                    <div class="dd-handle"> {{$categoryChild->name}} </div>
                                    @if($categoryChild->categoryChildren->count())
                                        <ol class="dd-list">
                                            @foreach($categoryChild->categoryChildren as $categoryChildItem)
                                                <li class="dd-item" data-id="3">
                                                    <div class="dd-handle"> {{$categoryChildItem->name}} </div>
                                                </li>
                                            @endforeach
                                        </ol>
                                    @endif
                                </li>
                            @endforeach
                        </ol>
                    @endif
                </li>
            @endif
        @endforeach
    </ol>
</div>
