<div class="modal center-modal" id="modal-update-category" tabindex="-1" style="display: block; padding-right: 15px;" aria-modal="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form-update-category">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Update Category</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="message-error mb-20" style="display: none">
                        <span class="badge badge-danger"></span>
                    </div>
                    <div class="form-group">
                        <h5>Category Name<span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="name" class="form-control" id="name">
                        </div>
                    </div>
                    <div class="form-group list-parent" data-action="{{route('categories.list-parent')}}">
                    </div>
                    <div class="form-group validate">
                        <h5>Status <span class="text-danger">*</span></h5>
                        <fieldset class="controls">
                            <input name="active" type="radio" id="update_active" value="1" checked>
                            <label for="update_active">Active</label>
                        </fieldset>
                        <fieldset>
                            <input name="active" type="radio" id="update_inactive" value="0" >
                            <label for="update_inactive">InActive</label>
                        </fieldset>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image-5" name="banner_update[]">
                                <label class="custom-file-label" for="image-5">Choose file</label>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="visible-image visible-image-5">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-4">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image-6" name="banner_update[]">
                                <label class="custom-file-label" for="image-6">Choose file</label>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="visible-image visible-image-6">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-4">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image-7" name="banner_update[]">
                                <label class="custom-file-label" for="image-7">Choose file</label>
                            </div>
                        </div>
                        <div class="col-8 mt-3">
                            <div class="visible-image visible-image-7">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-4">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image-8" name="banner_update[]">
                                <label class="custom-file-label" for="image-8">Choose file</label>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="visible-image visible-image-8">
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-uniform" style="width: 100%;">
                    <button type="button" class="btn btn-rounded btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-rounded btn-primary float-right" id="btn-update-category" >Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
