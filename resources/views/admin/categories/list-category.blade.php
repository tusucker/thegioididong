<div class="box-body no-padding" id="list-category" data-action="{{route('categories.list')}}">
    <div class="table-responsive">
        <table class="table table-hover text-center" >
            <tbody>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>slug</th>
                <th>parent</th>
                <th>status</th>
                <th>created at</th>
                <th>action</th>
            </tr>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->name}}</td>
                    <td>{{$category->slug}}</td>
                    <td>{{$category->parent_id==0?'I am Dad':$category->categoryParent->name}}</td>
                    <td>
                        <span class="badge badge-pill {{$category->active==1?'badge-success':'badge-danger'}}">
                            {{$category->active==1?'Active':'Inactive'}}
                        </span>
                        @can('edit-category')
                            <a class="change-status"
                               href="#"
                               data-action="{{route('categories.change-status',$category->id)}}"
                            >
                                <i class="fa fa-fw fa-refresh"></i>
                            </a>
                        @endcan
                    </td>
                    <td>{{$category->created_at}}</td>
                    <td>
                        @can('edit-category')
                            <a href="#" class="text-dark mr-15 update-category" style="font-size: 30px"
                               data-action="{{route('categories.update',$category->id)}}"
                               data-url="{{route('categories.get-category',$category->id)}}">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>
                        @endcan
                        @can('delete-category')
                                <a href="#" class="text-secondary delete-category" style="font-size: 30px"
                                   data-action="{{route('categories.destroy',$category->id)}}">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!!  $categories->appends(['search' => $data_input_search])->render()!!}
{{--        {!!  $categories->appends(request()->query())->links()!!}--}}

    </div>
</div>
