<div class="form-group list-parent" data-action="{{route('categories.list-parent')}}">
    <h5>Category Parent <span class="text-danger">*</span></h5>
    <div class="controls">
        <select name="parent_id" class="form-control category-parent">
            <option value="0">I am Dad</option>
            @foreach($listCategoryParent as $category)
                <option value="{{$category->id}}">{{str_repeat('--',$category->level).$category->name}}</option>
            @endforeach
        </select>
    </div>
</div>
