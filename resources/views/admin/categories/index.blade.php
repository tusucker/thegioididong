@extends('layouts.master')

@section('title')
    <title>Incense Admin - Categories</title>
@endsection

@section('css')
    <link rel="stylesheet" href="css/category/index.css">
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-3">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Categories</h4>
                    </div>
                    <div class="box-body">
                        <div class="myadmin-dd" data-action="{{route('categories.tree-view')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">List Categories</h4>
                        @can('add-category')
                            <button type="button" class="btn btn-success ml-15 open-modal-add-category">Add Category
                            </button>
                        @endcan
                        <div class="box-controls pull-right">
                            <div class="lookup lookup-circle lookup-right">
                                <form id="form-search">
                                    <label>
                                        <input type="text" name="search" id="search">
                                    </label>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding" id="list-category" data-action="{{route('categories.list')}}">
                    </div>
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

    @include('admin.categories.modal-add')
    @include('admin.categories.modal-update')
@endsection

@section('js')
    <script src="js/admin/category.js"></script>
@endsection



