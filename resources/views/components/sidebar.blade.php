<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <div class="user-profile">
            <div class="ulogo">
                <a href="index.html">
                    <!-- logo for regular state and mobile devices -->
                    <div class="d-flex align-items-center justify-content-center">
                        <img src="html/images/logo-dark.png" alt="">
                        <h3><b>Sunny</b> Admin</h3>
                    </div>
                </a>
            </div>
        </div>

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">
            @can('view-dashboard')
                <li class="{{ Session::get('isActive') == 1 ? 'active' : ''}}">
                    <a href="{{route('admin.dashboard')}}">
                        <i class="fa fa-fw fa-apple"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            @endcan
            @can('view-category')
                <li class="{{ Session::get('isActive') == 2 ? 'active' : ''}}">
                    <a href="{{route('categories.index')}}">
                        <i class="fa fa-fw fa-linux"></i>
                        <span>Categories</span>
                    </a>
                </li>
            @endcan
            @can('view-product')
                <li class="{{ Session::get('isActive') == 3 ? 'active' : ''}}">
                    <a href="{{route('products.index')}}">
                        <i class="fa fa-ravelry" aria-hidden="true"></i>
                        <span>Products</span>
                    </a>
                </li>
            @endcan
            @can('view-article')
                <li class="{{ Session::get('isActive') == 4 ? 'active' : ''}}">
                    <a href="{{route('articles.index')}}">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                        <span>Articles</span>
                    </a>
                </li>
            @endcan
            @can('view-user')
                <li class="{{ Session::get('isActive') == 5 ? 'active' : ''}}">
                    <a href="{{route('users.index')}}">
                        <i class="fa fa-futbol-o" aria-hidden="true"></i>
                        <span>Users</span>
                    </a>
                </li>
            @endcan
            @can('view-role')
                <li class="{{ Session::get('isActive') == 6 ? 'active' : ''}}">
                    <a href="{{route('roles.index')}}">
                        <i class="fa fa-music" aria-hidden="true"></i>
                        <span>Roles</span>
                    </a>
                </li>
            @endcan
                <li class="{{ Session::get('isActive') == 7 ? 'active' : ''}}">
                    <a href="{{route('orders.index')}}">
                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                        <span>Invoice</span>
                    </a>
                </li>
        </ul>
    </section>

    <div class="sidebar-footer">
        <!-- item-->
        <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings"
           aria-describedby="tooltip92529"><i class="ti-settings"></i></a>
        <!-- item-->
        <a href="mailbox_inbox.html" class="link" data-toggle="tooltip" title="" data-original-title="Email"><i
                class="ti-email"></i></a>
        <!-- item-->
        <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><i
                class="ti-lock"></i></a>
    </div>
</aside>
