@if (session()->has('errors'))
    <div class="alert alert-danger">
        @if(is_array(session('errors')))
            <ul>
                @foreach (session('errors') as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        @else
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        @endif
    </div>
@endif