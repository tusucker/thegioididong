<div class="content-wrapper">
    <div class="container-full">

        <!-- Main content -->
        @yield('content')
        <!-- /.content -->
    </div>
</div>
