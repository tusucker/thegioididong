<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
    <base href="{{ asset('') }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/setup.css"/>
    <link rel="stylesheet" type="text/css" href="vendor/all.min.css"/>
    <link rel="stylesheet" type="text/css" href="store/style/css/style-main.css"/>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans" media="all"/>
    <title>T-H</title>
    @yield('css')
</head>
<body class="home home-01 product-view">
<!--begin header-->
@include('components-customer.header')
<!--end header-->
<!--begin content-->
@yield('content')
<!--end content-->
<!--begin footer-->
@include('components-customer.footer')
@include('components-customer.form-login')
@include('components-customer.form-register')
@include('components-customer.modal-profile-user')
<div style="z-index: 100000" class="modal fade" id="modalWishList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
</div>
<div style="z-index: 100001" class="modal fade" id="modalDetailOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
</div>
<!--end footer-->
<script src="vendor/all.min.js"></script>
<script src="js/vendor/sweetalert2@10.js"></script>
<script src="js/setup-frontend.js"></script>
@yield('js')
</body>
</html>
