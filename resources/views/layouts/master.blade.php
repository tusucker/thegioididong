<!DOCTYPE html>
<html lang="en">
<head>
    <base href="{{asset('')}}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="html/images/favicon.ico">

    @yield('title')

    <!-- Vendors Style-->
    <link rel="stylesheet" href="html/main/css/vendors_css.css">

    <!-- Style-->
    <link rel="stylesheet" href="html/main/css/style.css">
    <link rel="stylesheet" href="html/main/css/skin_color.css">
    <link rel="stylesheet" href="css/setup.css">
    <link rel="stylesheet" href="css/vendor/select2.min.css">

    @yield('css')

</head>

<body class="hold-transition light-skin sidebar-mini theme-primary fixed">
    <div class="wrapper">
        @include('components.header')
        <!-- Left side column. contains the logo and sidebar -->
        @include('components.sidebar')
        <!-- Content Wrapper. Contains page content -->
        @include('components.content')
        <!-- /.content-wrapper -->
        @include('components.footer')
    </div>
    <!-- ./wrapper -->
    <!-- Vendor JS -->
    <script src="html/main/js/vendors.min.js"></script>
    <script src="html/assets/icons/feather-icons/feather.min.js"></script>
    <!-- Sunny Admin App -->
    <script src="html/main/js/template.js"></script>
    <script src="js/vendor/sweetalert2@10.js"></script>
    <script src="js/vendor/select2.min.js"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="js/setup.js"></script>
    @yield('js')
</body>
</html>
