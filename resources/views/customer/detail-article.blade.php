@extends('layouts.master-customer')

@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <ul>
                    <li class="home">
                        <a href="{{ route('frontend.index') }}" title="Go to Home Page">Home</a>
                        <span>|</span>
                    </li>
                    <li class="category3">
                        <a href="{{ route('frontend.list-article') }}">Bài viết</a>
                        <span>|</span>
                    </li>
                    <li class="category3">
                        <strong>{{ $article->name }}</strong>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <!-- Title -->
                <h1 class="mt-4" style="font-weight: bolder; color: black">{{ $article->name }}</h1>

                <!-- Author -->
                <p class="lead">
                    by
                    <a href="#">{{ $article->user->name }}</a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p>Posted on {{ $article->created_at }}</p>

                <hr>

                <!-- Preview Image -->
                <img class="img-fluid rounded" src="{{ $article->avatar }}" alt="">

                <hr>

                <!-- Post Content -->
                <div>
                    {!! $article->content !!}
                </div>
                <hr>
                <!-- Comments Form -->
                <div class="card my-4">
                    <h5 class="card-header" style="color: black">Để lại bình luận bên dưới</h5>
                    <div class="card-body">
                        <form>
                            @csrf
                            <input type="hidden" class="id-user" value="{{ \Auth::id() ?? 0 }}">
                            <div class="form-group">
                                <textarea class="form-control content" rows="3" ></textarea>
                            </div>
                            <button type="button" class="btn btn-primary send-comment"
                                    data-action="{{ route('frontend.send-comment', ['id' => $article->id, 'parentId' => 0]) }}">
                                Gửi
                            </button>
                        </form>
                    </div>
                </div>

                <!-- Single Comment -->
                @php
                    use Carbon\Carbon;
                    Carbon::setLocale('vi');
                    $now = Carbon::now();
                @endphp
                <div class="total-comment">Tổng số bình luận: {{ $listArticleComment->count() }}</div>
                @foreach($listArticleComment as $key => $comment)
                    @if($key <= 10 && $comment->parent_id == 0)
                        <div class="media mb-4">
                            <div class="media-body">
                                <span class="mt-0" style="color: black; font-size: 18px">{{ $comment->user->name }}</span> <span>{{ $comment->time }}</span>
                                <div style="margin-top: 12px; margin-bottom: 20px">{!! nl2br($comment->content) !!} </div>
                                <hr>

                                @php
                                    $listChildren = $comment->articleCommentChildren->reverse();
                                @endphp

                                @foreach($listChildren as $children)
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <div><span style="color: black; margin-right: 10px">{{ $children->user->name }}</span><span>{{ $children->created_at->diffForHumans($now) }}</span></div>
                                            <div>{!! nl2br($children->content) !!}</div>
                                            <hr>
                                        </div>
                                    </div>
                                @endforeach
                                <span class="answer"><small style="color: #4a90e2; cursor: pointer">Trả lời</small></span>
                            </div>
                            <div style="display: none">
                                <div class="form-group" style="margin-top: 5px">
                                    <textarea class="form-control content" rows="3"></textarea>
                                </div>
                                <button type="button" class="btn btn-primary send-comment"
                                        data-action="{{ route('frontend.send-comment',
                                                        ['id' => $article->id, 'parentId' => $comment->id]
                                                        ) }}">
                                    Gửi
                                </button>
                            </div>
                            <hr>
                        </div>
                    @endif
                @endforeach
            </div>

            <!-- Sidebar Widgets Column -->
            @include('components-customer.list-article.content-right')
        </div>
        <!-- /.row -->

    </div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="css/frontend/list-blog.css"/>
    <link rel="stylesheet" type="text/css" href="css/frontend/blog-detail.css"/>
@endsection

@section('js')
    <script src="js/frontend/detail-article.js"></script>
@endsection