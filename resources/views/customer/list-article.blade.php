@extends('layouts.master-customer')

@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <ul>
                    <li class="home">
                        <a href="{{ route('frontend.index') }}" title="Go to Home Page">Home</a>
                        <span>|</span>
                    </li>
                    <li class="category3">
                        <strong>Bài viết</strong>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <h1 class="my-4 title-page">BLOG
                    <hr style="border: 1px solid black">
                </h1>

                <!-- Blog Post -->
                @foreach($listArticle as $article)
                    <div class="row card blog-simple">
                        <div class="card-body">
                            <div class="col-md-4">
                                <img class="card-img-top" src="{{ $article->avatar }}" alt="Card image cap">
                            </div>
                            <div class="col-md-8">
                                <h2 class="card-title">
                                    <a href="{{ route('frontend.detail-article', ['id' => $article->id]) }}">{{ $article->name }}</a>
                                </h2>
                                <div class="card-text">
                                    {!! $article->description !!}
                                </div>
                                <div>
                                    Posted on <span style="color: black">{{ $article->created_at }}</span> by <span style="color: black; font-weight: bolder">{{ $article->user->name }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <!-- Pagination -->
            </div>

            <!-- Sidebar Widgets Column -->
            @include('components-customer.list-article.content-right')
        </div>
        <!-- /.row -->

    </div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="css/frontend/list-blog.css"/>
@endsection

@section('js')
    <script src="js/frontend/cart-detail.js"></script>
@endsection