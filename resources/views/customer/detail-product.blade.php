@extends('layouts.master-customer')

@section('content')
    <div class="content">
        @include('components-customer.list-product.beadcrumbs')
        <div class="container">
            <div class="row">
                <div class="row">
                    @include('components-customer.detail-product.box-left')
                    @include('components-customer.detail-product.main')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')

@endsection

@section('js')
    <script src="js/frontend/detail-product.js"></script>
@endsection