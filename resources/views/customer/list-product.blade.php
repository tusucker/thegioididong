@extends('layouts.master-customer')

@section('content')
    <div class="content">
        @include('components-customer.list-product.beadcrumbs')
        <div class="container">
            <div class="row">
                <div class="row">
                   @include('components-customer.list-product.box-left')
                   @include('components-customer.list-product.main')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="css/frontend/list-product.css"/>
@endsection

@section('js')
    <script src="js/frontend/list-product.js"></script>
@endsection