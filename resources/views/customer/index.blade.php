@extends('layouts.master-customer')

@section('content')
    <div id="box-content">
        @include('components-customer.home-content.container')
        @include('components-customer.home-content.position-10')
        @include('components-customer.home-content.position-03')
        @include('components-customer.home-content.position-02')
        @include('components-customer.home-content.position-04')
        @include('components-customer.home-content.position-06')
        @include('components-customer.home-content.position-08')
        @include('components-customer.home-content.position-09')
    </div>
@endsection