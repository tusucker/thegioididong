@extends('layouts.master-customer')

@section('content')
    <div class="card">
        <input type="hidden" id="alert-message" value="{{ \Session::get('alert-message') ?? 2 }}">
        @php
            \Session::put('alert-message', 2);
        @endphp
        <div class="row">
            <div class="col-md-8 cart">
                <div class="title">
                    <div class="row">
                        <div class="col">
                            <h4><b>Shopping Cart</b></h4>
                        </div>
                        <div class="col align-self-center text-right text-muted">{{ Cart::count() }} SẢN PHẨM</div>
                    </div>
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr style="font-weight: bolder; color: black">
                        <th scope="col"></th>
                        <th scope="col">Sản phẩm</th>
                        <th scope="col">Số lượng</th>
                        <th scope="col">Đơn giá</th>
                        <th scope="col">Thành tiền</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(Cart::content() as $item)
                        <tr>
                            <input type="hidden" value="{{ $item->rowId }}">
                            <th scope="row">
                                <img class="avatar-product" src="{{ $item->options->image }}">
                            </th>
                            <td style="width: 100px">{{ $item->name }}</td>
                            <td>
                                <input style="width: 50px" type="number" value="{{ $item->qty }}">
                                <a class="update-quantity" href="{{ route('frontend.update-quantity-product-from-cart', ['rowId' => $item->rowId]) }}" style="display: block; margin-left: -10px; font-size: 11px ">Cập nhật</a>
                            </td>
                            <td>
                                <div class="price-box">
                                    <div style="font-weight: bolder; color: black !important;">{{ number_format( $item->price,2,",",".") }}
                                        VND
                                    </div>
                                    @if($item->options->sale > 0)
                                        <div>Sale: - {{ $item->options->sale }} %</div>
                                        <p class="special-price">
                                            <span class="price">{{ number_format($item->options->price_origin,2,",",".") }} VND</span>
                                        </p>
                                    @endif
                                </div>
                            </td>
                            <td>
                                <div class="price-box" style="font-weight: bolder; color: black !important;">
                                    <div>{{ number_format($item->qty * $item->price ,2,",",".") }} VND</div>
                                </div>
                            </td>
                            <td>
                                <a href="{{ route('frontend.remove-product-from-cart', ['rowId' => $item->rowId]) }}">Xoá</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row" >
                    <div class="col" style="color: black; font-weight: bolder; text-transform: uppercase; margin-top: 50px">
                        <h4>Tổng tiền cần thanh toán</h4>
                    </div>
                    <div class="col text-right" style="color: black; font-weight: bolder">{{ Cart::subtotal() }} VND</div>
                </div>
                <div class="back-to-shop"><a href="{{ route('frontend.index') }}">&leftarrow;</a><span class="text-muted">Back to shop</span></div>
            </div>
            <div class="col-md-4 summary">
                <form method="POST" action="{{ route('frontend.checkout') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Họ và tên</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Họ và tên" value="{{ old('name') }}">
                        <div class="text-danger font-size-h6"> {!!$errors->first('name')!!}</div>
                    </div>
                    <div class="form-group">
                        <label for="phone">Số điện thoại</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Số điện thoại" value="{{ old('phone') }}">
                        <div class="text-danger font-size-h6"> {!!$errors->first('phone')!!}</div>
                    </div>
                    <div class="form-group">
                        <label for="address">Địa chỉ</label>
                        <input type="text" class="form-control" id="address" name="address" placeholder="Địa chỉ" value="{{ old('address') }}">
                        <div class="text-danger font-size-h6"> {!!$errors->first('address')!!}</div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
                        <div class="text-danger font-size-h6"> {!!$errors->first('email')!!}</div>
                    </div>
                    <div class="form-group">
                        <label for="note">Ghi chú thêm</label>
                        <textarea name="note" id="note" cols="30" rows="5">{{ old('note') }}</textarea>
                    </div>
                    <button style="font-size: 13px;
                                font-weight: bolder;
                                background-color: #000;
                                border-color: #000;
                                color: white;
                                width: 100%;
                                margin-top: 4vh;
                                padding: 1vh;
                                border-radius: 0" class="btn" type="submit">
                        Đặt hàng
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="css/frontend/cart-detail.css"/>
@endsection

@section('js')
    <script src="js/frontend/cart-detail.js"></script>
@endsection