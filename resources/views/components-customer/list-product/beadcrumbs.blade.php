<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <ul>
                <li class="home">
                    <a href="{{ route('frontend.index') }}" title="Go to Home Page">Home</a>
                    <span>|</span>
                </li>
                <li class="category3">
                    <strong>{{ $categorySelected->name }}</strong>
                </li>
            </ul>
        </div>
    </div>
</div>