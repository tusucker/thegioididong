<div id="box-left" class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
    <div class="block block-layered-nav">
        <div class="block-title">
            <strong><span>{{ $categorySelected->name }}</span></strong>
        </div>
        <div class="block-content">
            <dl id="narrow-by-list">
                <dt style="width: 268px" class="category-children active-category"
                    data-action="{{ route('frontend.show-product', ['id' => 0, 'parentId' => $id]) }}">
                    Tất cả
                </dt>
                @foreach($categorySelected->categoryChildren as $key => $category)
                    <dt style="width: 268px" class="category-children"
                        data-action="{{ route('frontend.show-product', ['id' => $category->id, 'parentId' => $id]) }}">
                        {{ $category->name }}
                    </dt>
                @endforeach
            </dl>
        </div>
        <div class="block-title-shop-by">
            <strong><span>Shop by</span></strong>
        </div>
        <div class="block-content">
            <dl id="narrow-by-list">
                <dt id="tab2" class="tab-accordion accordion-open"> Price</dt>
                <dd class="tabcontent2">
                    <div class="price">
                        <div class="range-wrap"><div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header" style="left: 0%; width: 100%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 100%;"></a></div>
                        </div>
                        <div class="text-box"><div class="price-from">
                                $ <input type="text" id="minPrice" class="priceTextBox" value="0"></div>
                            <div class="price-to">
                                <input type="text" id="maxPrice" class="priceTextBox" value="30000000" style="width: 85px !important;">
                            </div>
                            <a class="go fillter"
                               href="javascript:void(0)"
                               style="margin-left: 0px !important; margin-top: 10px !important;"
                               data-action="{{ route('frontend.show-product-by-price', ['parentId' => $id]) }}"
                            >
                                Fillter
                            </a>
                            <input type="hidden" id="amount" readonly="readonly" style="background:none; border:none;" value="$0 - $30000000">
                        </div>
                    </div>
                </dd>
            </dl>
        </div>
    </div>
    <div class="block box-banner">
        <div id="box-banner" class="carousel slide" data-ride="carousel" data-interval="1500"><ol class="carousel-indicators">
                <li class="" data-target="#box-banner" data-slide-to="0"></li>
                <li data-target="#box-banner" data-slide-to="1" class="active"></li>
                <li data-target="#box-banner" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner">
                <div class="item"><a href="detail.html"><img src="store/images/banner13.png" alt="banner"></a>
                    <div class="std">
                        <span class="t1">up to 45%</span>
                        <span class="t2">Nokia Lumia 920</span>
                        <span class="t3">At verov eos et accusamus et iusto ods un dignissimos ducimus qui blan ditiis prasixer esentium</span>
                    </div>
                    <a class="gt-shop" href="#">Shop Now</a>
                </div>
                <div class="item active"><a href="detail.html"><img src="store/images/banner14.png" alt="banner"></a>
                    <div class="std">
                        <span class="t1">up to 50%</span>
                        <span class="t2">Nokia Lumia 920</span>
                        <span class="t3">At verov eos et accusamus et iusto ods un dignissimos ducimus qui blan ditiis prasixer esentium</span>
                    </div>
                    <a class="gt-shop" href="#">Shop Now</a>

                </div>
                <div class="item"><a href="detail.html"><img src="store/images/banner15.png" alt="banner"></a>
                    <div class="std">
                        <span class="t1">up to 80%</span>
                        <span class="t2">Nokia Lumia 920</span>
                        <span class="t3">At verov eos et accusamus et iusto ods un dignissimos ducimus qui blan ditiis prasixer esentium</span>
                    </div>
                    <a class="gt-shop" href="#">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>