<div id="main" class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
    <div class="wrap-banner-cate">
        <div class="cate-img">
            <img src="{{ $categorySelected->banners->count() ? $categorySelected->banners[0]->path : 'store/images/banner-2.png' }} " alt="">
        </div>
    </div>
    <div class="page-title category-title">
        <h1>Mobile</h1>
    </div>
    <div id="catalog-listing">
        <div class="category-products page-product-list">
            <div class="toolbar-top">
                <div class="toolbar">
                    <div class="pager">
                        <div class="pages">

                        </div>
                        <!--<label class="item-pp"></label>-->
                    </div>
                </div>
            </div>
            <ul class="products-grid row">
                @foreach($listProduct as $product)
                    <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item">
                        <div class="item-wrap">
                            <div class="item-image">
                                <a class="product-image no-touch" href="#" title="{{ $product->name }}">
                                    <img style="width: 165px; height: 165px" class="first_image" src="{{ $product->avatar }}" alt="Product demo">
                                </a>
                                <div class="item-btn">
                                    <div class="box-inner">
                                        @if(\Auth::check())
                                            <a title="Add to wishlist" href="{{ route('frontend.add-to-wishlist', ['id' => $product->id]) }}"
                                               class="link-wishlist" style="background: {{ $listWishList->contains($product->id) ? 'yellow' : ''}} ">&nbsp;</a>
                                        @else
                                            <a title="Add to wishlist" href="javascript:void();" class="open-login-form link-wishlist">&nbsp;</a>
                                        @endif
                                        <span class="qview"><a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}"></a>
										  <a class="vt_quickview_handler"
                                             data-original-title="Quick View"
                                             data-placement="left"
                                             data-toggle="tooltip"
                                             href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">
                                              <span>Quick View</span>
                                          </a>
                                        </span>
                                    </div>
                                </div>
                                <a title="Add to cart" class="btn-cart" href="{{ route('frontend.add-to-cart-simple', ['productId' => $product->id]) }}">&nbsp;</a>
                            </div>
                            <div class="pro-info">
                                <div class="pro-inner">
                                    <div class="pro-title product-name"><a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">{{ $product->name }}</a></div>
                                    <div class="pro-content">
                                        <div class="wrap-price">
                                            <div class="price-box">
												<span class="regular-price">
												<span class="price">{{ number_format($product->price,2,",",".") }} VND</span></span>
                                            </div>
                                        </div>
                                        <div class="ratings">
                                            <div class="rating-box">
                                                <div class="rating" style="width: {{ $product->average_star * 20 }}%"></div>
                                            </div>
                                            <span class="amount"><a href="javascript:void();">{{ $product->times_rating }} đánh giá</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end item wrap -->
                    </li>
                @endforeach
            </ul>
            <div class="toolbar-bottom">
                <div class="toolbar">
                    <div class="pager">
                        <div class="pages">
                            <strong>Pages</strong>
                            <ol>
                                <li class="current">1</li>
                                <li><a href="grid.html?p=2">2</a></li>
                                <li>
                                    <a class="next" href="grid.html?p=2" title="Next">
                                        &nbsp;                                 </a>
                                </li>
                            </ol>
                        </div>
                        <!--<label class="item-pp"></label>-->
                    </div>
                    <div class="sorter">
                        <label>View as</label>
                        <p class="view-mode">
                            <strong title="Grid" class="grid">Grid</strong>
                            <a href="grid.html?mode=list" title="List" class="list">List</a>
                        </p>
                        <div class="sort-by">
                            <label>Sort By</label>
                            <div class="wrap-sb">
                                <div class="selected-order">Position</div>
                                <ul class="select-order">
                                    <li><a href="grid.html?dir=asc&amp;order=position" class="selected">Position</a></li>
                                    <li><a href="grid.html?dir=asc&amp;order=name">Name</a></li>
                                    <li><a href="grid.html?dir=asc&amp;order=price">Price</a></li>
                                </ul>
                            </div>
                            <a class="desc" href="grid.html?dir=desc&amp;order=position" title="Set Descending Direction">
                                <!--<img src="" alt="" class="v-middle" />-->
                            </a>
                        </div>
                        <div class="limiter">
                            <label>Show</label>
                            <div class="wrap-show">
                                <div class="selected-limiter">9</div>
                                <ul class="select-limiter">
                                    <li><a href="grid.html?limit=9" class="selected">9</a></li>
                                    <li><a href="grid.html?limit=12">12</a></li>
                                    <li><a href="grid.html?limit=24">24</a></li>
                                    <li><a href="grid.html?limit=36">36</a></li>
                                </ul>
                            </div>
                            <label class="stylepp">per page</label>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>