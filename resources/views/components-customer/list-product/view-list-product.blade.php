<ul class="products-grid row">
    @foreach($listProduct as $product)
        <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item">
            <div class="item-wrap">
                <div class="item-image">
                    <a class="product-image no-touch" href="#" title="{{ $product->name }}">
                        <img style="width: 165px; height: 165px" class="first_image" src="{{ $product->avatar }}" alt="Product demo">
                    </a>
                    <div class="item-btn">
                        <div class="box-inner">
                            @if(\Auth::check())
                                <a title="Add to wishlist" href="{{ route('frontend.add-to-wishlist', ['id' => $product->id]) }}"
                                   class="link-wishlist" style="background: {{ $listWishList->contains($product->id) ? 'yellow' : ''}} ">&nbsp;</a>
                            @else
                                <a title="Add to wishlist" href="javascript:void();" class="open-login-form link-wishlist">&nbsp;</a>
                            @endif
                            <span class="qview">
                                <a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}"></a>
                                <a class="vt_quickview_handler"
                                   data-original-title="Quick View"
                                   data-placement="left"
                                   data-toggle="tooltip"
                                   href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">
                                    <span>Quick View</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <a title="Add to cart" class="btn-cart" href="{{ route('frontend.add-to-cart-simple', ['productId' => $product->id]) }}">&nbsp;</a>
                </div>
                <div class="pro-info">
                    <div class="pro-inner">
                        <div class="pro-title product-name"><a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">{{ $product->name }}</a></div>
                        <div class="pro-content">
                            <div class="wrap-price">
                                <div class="price-box">
												<span class="regular-price">
												<span class="price">{{ number_format($product->price,2,",",".") }} VND</span></span>
                                </div>
                            </div>
                            <div class="ratings">
                                <div class="rating-box">
                                    <div class="rating" style="width: {{ $product->average_star * 20 }}%"></div>
                                </div>
                                <span class="amount"><a href="javascript:void();">{{ $product->times_rating }} đánh giá</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end item wrap -->
        </li>
    @endforeach
</ul>