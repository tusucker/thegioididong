<div id="main" class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
    <div class="product-essential">
        <div class="wrap">
            <form action="#" method="post" id="product_addtocart_form">
                <div class="product-name">
                    <h1>{{ $productSelected->name }}</h1>
                </div>
                <div class="product-img-box">
                    <div class="image-main">
                        <img src="{{ $productSelected->avatar }}" alt="Product demo" />
                    </div>
                    <div class="click-quick-view">&nbsp;</div>
                    <div id="galary-image" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                @foreach($productSelected->images as $image)
                                    <div class="sub-item">
                                        <img src="{{ $image->path }}" alt="Flower">
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#galary-image" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#galary-image" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <div class="image-quick-view no-display">

                        <div class="content">
                            <span class="close">x</span>
                            <img src="store/images/product/larg/demo6.jpg" alt=""/>
                        </div>
                    </div>
                </div>
                <div class="product-shop">
                    <div class="wrap-er">
                        <div class="ratings">
                            <div class="rating-box">
                                <div class="rating" style="width: {{ $productSelected->average_star * 20 }}%"></div>
                            </div>
                            <p class="rating-links">
                                <a href="javascript: void();">({{ $productSelected->times_rating }} đánh giá)</a>
                                <span class="separator">|</span>
                                <a class="re-temp" href="#">Add Your Review</a>
                            </p>
                        </div>
                    </div>
                    <!--<div class="product-code"><label>Product code:</label></div>-->
                    <div class="short-description">
                        <div class="std">
                            <span>{{ $productSelected->description }}</span>
                        </div>
                        <div class="box-info-detail">
                            @if($productSelected->quantity > 0)
                                <p class="availability in-stock">
                                    <span class="label">Availability:</span>
                                    <span class="value">In stock</span>
                                </p>
                            @else
                                <p class="availability in-stock" style="border: 1px solid gainsboro;
                                                                    padding: 7px;
                                                                    background: yellow;
                                                                    color: red;
                                                                    font-weight: bold;">
                                    Hết Hàng
                                </p>
                            @endif
                            <div class="price-info">
                                <div class="price-box">
                                    <p class="old-price">
                                        <span class="price">{{ number_format($productSelected->price - $productSelected->price * $productSelected->sale / 100 ,2,",",".") }} VND</span>
                                    </p>
                                    <p class="special-price {{$productSelected->sale == 0 ? 'd-none' : ''}}">
                                        <span class="price">{{ number_format($productSelected->price,2,",",".") }} VND</span>
                                    </p>
                                </div>
                            </div>
                            <div class="product-options-bottom">
                                @if($productSelected->quantity > 0)
                                    <div class="add-to-box add-to-cart">
                                        <div class="add-to-cart">
                                            <div class="add-to-cart-buttons">
                                                <button type="button" title="Add to Cart" class="button btn-cart"
                                                        data-product="{{ $productSelected }}">
                                                    <span><span>Add to Cart</span></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="add-to-box-sub">
                                    <ul class="add-to-links">
                                        <li><a href="#" class="link-wishlist">Add to Wishlist</a></li>
                                        <li><a href="#" class="link-compare">Add to Compare</a></li>
                                        <li><a class="email-friend" href="#">Email to a Friend</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--div class="product-options-bottom">
                                <div class="add-to-box add-to-cart">
                                                                </div>
                                </div-->
                        </div>
                        <!-- end div .box-info-detail -->
                    </div>
                </div>
            </form>
            <div class="share-this" style="margin-top:18px;">
                <script type="text/javascript">var switchTo5x=true;</script>
                <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
                <script type="text/javascript">stLight.options({publisher: "6d56a077-95db-44a6-a019-d5a901534fea", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
            </div>
        </div>
        <div class="tab-detail">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Description</a></li>
                <li><a data-toggle="tab" href="#menu2">Review</a></li>
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <p>{!! $productSelected->content !!}</p>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <div class="form-add">
                        <h2>Gửi đánh giá của bạn</h2>
                        <form action="{{ route('frontend.send-comment-product', ['id' => $productSelected->id, 'parentId' => 0]) }}" id="form-send-comment">
                            @csrf
                            <input type="hidden" class="id-user" value="{{ \Auth::id() ?? 0 }}">
                            <h3>Sản phẩm đánh giá:  <span>{{ $productSelected->name }}</span>
                            </h3>
                            <div class="fieldset">
                                <span id="input-message-box"></span>
                                <table class="data-table review-summary-table ratings" id="product-review-table">
                                    <col width="1" />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <thead>
                                    <tr>
                                        <th style="width: 20%">
                                            <div class="rating-box">
                                                <span class="rating nobr" style="width:20%;"></span>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="rating-box">
                                                <span class="rating nobr" style="width:40%;"></span>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="rating-box">
                                                <span class="rating nobr" style="width:60%;"></span>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="rating-box">
                                                <span class="rating nobr" style="width:80%;"></span>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="rating-box">
                                                <span class="rating nobr" style="width:100%;"></span>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="value"><label for="Quality_1"><input type="radio" name="rating_number" id="Quality_1" value="1" class="radio" /></label></td>
                                        <td class="value"><label for="Quality_2"><input type="radio" name="rating_number" id="Quality_2" value="2" class="radio" /></label></td>
                                        <td class="value"><label for="Quality_3"><input type="radio" name="rating_number" id="Quality_3" value="3" class="radio" /></label></td>
                                        <td class="value"><label for="Quality_4"><input type="radio" name="rating_number" id="Quality_4" value="4" class="radio" /></label></td>
                                        <td class="value"><label for="Quality_5"><input type="radio" name="rating_number" id="Quality_5" value="5" class="radio" /></label></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <ul class="form-list">
                                    <li>
                                        <label for="review_field" class="required" style="margin-top: 10px">Mời bạn để lại bình luận</label>
                                        <div class="input-box">
                                            <textarea name="content" id="review_field" cols="5" rows="3" class="required-entry"></textarea>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="buttons-set">
                                <button type="button" title="Submit Review" class="button send-comment-product"><span><span>Bình luận</span></span></button>
                            </div>
                        </form>
                        @php
                            use Carbon\Carbon;
                            Carbon::setLocale('vi');
                            $now = Carbon::now();
                        @endphp
                        <div class="total-comment">Tổng số bình luận: {{ $listProductComment->count() }}</div>
                        @foreach($listProductComment as $key => $comment)
                            @if($key <= 10 && $comment->parent_id == 0)
                                <div class="media mb-4">
                                    <div class="media-body">
                                        <span class="mt-0" style="color: black; font-size: 18px">{{ $comment->user->name }}</span> <span>{{ $comment->time }}</span>
                                        <div class="ratings">
                                            <div class="rating-box">
                                                <div class="rating" style="width: {{ $comment->rating_number * 20 }}%"></div>
                                            </div>
                                        </div>
                                        <div style="margin-top: 12px; margin-bottom: 20px">{!! nl2br($comment->content) !!}</div>
                                        <hr>
                                        @php
                                            $listChildren = $comment->ratingChildren->reverse();
                                        @endphp

                                        @foreach($listChildren as $children)
                                            <div class="row" style="margin-bottom: 10px">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                    <div>
                                                        <span style="color: black; margin-right: 10px">{{ $children->user->name }}</span>
                                                        <span>{{ $children->created_at->diffForHumans($now) }}</span>
                                                    </div>
                                                    <div>{!! nl2br($children->content) !!}</div>
                                                    <hr>
                                                </div>
                                            </div>
                                        @endforeach
                                        <span class="answer-comment-product"><small style="color: #4a90e2; cursor: pointer">Phản hồi</small></span>
                                    </div>
                                    <div style="display: none">
                                        <div class="form-group" style="margin-top: 5px">
                                            <textarea class="form-control content" rows="3"></textarea>
                                        </div>
                                        <button type="button"
                                                class="btn btn-primary send-comment-simple"
                                                data-action="{{ route('frontend.send-comment-product-non-rating',
                                                                ['id' => $productSelected->id, 'parentId' => $comment->id]
                                                                ) }}">
                                            Phản hồi
                                        </button>
                                    </div>
                                    <hr>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="upsell_pro" class="products-grid">
        <div class="inner">
            <div class="title">
                <span>Sản phẩm liên quan</span>
            </div>
            <div class="block  vt-slider vt-slider5  row">
                <div class="slider-inner">
                    <div class="container-slider">
                        <div class="products-grid">
                            @foreach($listUpsellProducts as $upsellProduct)
                                <div class="item">
                                    <div class="item-wrap">
                                        <div class="item-image">
                                            <a class="product-image no-touch" href="#" title="Ipad Air and iOS7">
                                                <img class="first_image" src="{{ $upsellProduct->avatar }}" alt="Product demo" />
                                            </a>
                                            <div class="item-btn">
                                                <div class="box-inner">
                                                    @if(\Auth::check())
                                                        <a title="Add to wishlist" href="{{ route('frontend.add-to-wishlist', ['id' => $upsellProduct->id]) }}"
                                                           class="link-wishlist" style="background: {{ $listWishList->contains($upsellProduct->id) ? 'yellow' : ''}} ">&nbsp;</a>
                                                    @else
                                                        <a title="Add to wishlist" href="javascript:void();" class="open-login-form link-wishlist">&nbsp;</a>
                                                    @endif
                                                    <span class="qview">
													<a class="vt_quickview_handler"
                                                       data-original-title="Quick View"
                                                       data-placement="left"
                                                       data-toggle="tooltip"
                                                       href="{{ route('frontend.show-detail-product', ['id' => $upsellProduct->id, 'parentId' => $categorySelected->id]) }}">
                                                        <span>Quick View</span>
                                                    </a>
												   </span>
                                                </div>
                                            </div>
                                            <a title="Add to cart" class="btn-cart" href="#">&nbsp;</a>
                                        </div>
                                        <div class="pro-info">
                                            <div class="pro-inner">
                                                <div class="pro-title product-name"><a href="{{ route('frontend.show-detail-product', ['id' => $upsellProduct->id, 'parentId' => $categorySelected->id]) }}">{{ $upsellProduct->name }}</a></div>
                                                <div class="pro-content">
                                                    <div class="wrap-price">
                                                        <div class="price-box">
														    <span class="regular-price">
                                                                <span class="price">{{ number_format($upsellProduct->price - $upsellProduct->price * $upsellProduct->sale / 100 ,2,",",".") }} VND</span>
                                                             </span>
                                                        </div>
                                                    </div>
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <div class="rating" style="width: {{ $upsellProduct->average_star * 20 }}%"></div>
                                                        </div>
                                                        <span class="amount"><a href="javascript:void();">{{ $upsellProduct->times_rating }} đánh giá</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end item wrap -->
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="navslider">
                        <a class="prev" href="#">Prev</a>
                        <a class="next" href="#">Next</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal-add-to-cart" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-modal="true" style="display: none; z-index: 100000;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title product-name" id="myModalLabel" style="font-weight: bolder; color: black">
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="price-box">
                <span style="font-weight: bolder; color: black">Gía sản phẩm: </span>
                 <span class="regular-price">
                    <span class="price price-product-real"></span> <span>VND</span>
                 </span>
                    <p class="special-price" >
                        <span class="price price-product-origin"></span> <span>VND</span>
                    </p>
                </div>
                <hr>
                <img class="avatar-product" style="overflow: hidden; max-width: 150px;  max-height: 150px; vertical-align: middle;" src="">

                <p style="margin-top: 20px;">
                    <span style="font-weight: bolder; color: black">Chọn số lượng:  </span>
                    <input type="number" value="1" class="quantity-product" style="width: 49px; padding: 9px; border-radius: 10px; border: 1px solid;">
                </p>
            </div>
            <div class="modal-footer">
                <button style="display: block;
                        overflow: hidden;
                        padding: 14px;
                        color: #fff;
                        text-transform: uppercase;
                        background: linear-gradient(180deg, #f79429 0%, #f7712e 100%);
                        width: 100%;
                        box-sizing: border-box;
                        border: 0;
                        border-radius: 4px;
                        font-weight: 600;
                        text-align: center;
                        margin: auto;
                        cursor: pointer;"
                        data-action="{{ route('frontend.add-to-cart') }}"
                        class="btn-add-to-cart"
                >Thêm vào giỏ hàng</button>
                <a href="{{ route('frontend.show-cart-detail') }}" class="seecart" style="display: block;
                    overflow: hidden;
                    padding: 10px 0 0;
                    text-align: center;
                    color: #288ad6;"
                >Xem giỏ hàng</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
