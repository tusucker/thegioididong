<div id="box-left" class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
    <div class="block block-lastest-pro">
        <div class="block-title">
            <strong><span>Latest Products</span></strong>
        </div>
        <div class="block  vt-slider lastest-slider  row">
            <div class="slider-inner">
                <div class="container-slider">
                    <div class="products-grid">
                        @foreach($listLatestProducts as $latestProduct)
                            <div class="item">
                                <div class="item-wrap">
                                    <div class="item-image">
                                        <a class="product-image no-touch" href="#" title="Ipad Air and iOS7">
                                            <img class="first_image" src="{{ $latestProduct->avatar }}" alt="Product demo" />
                                        </a>
                                        <div class="item-btn">
                                            <div class="box-inner">
                                                @if(\Auth::check())
                                                    <a title="Add to wishlist" href="{{ route('frontend.add-to-wishlist', ['id' => $latestProduct->id]) }}"
                                                       class="link-wishlist" style="background: {{ $listWishList->contains($latestProduct->id) ? 'yellow' : ''}} ">&nbsp;</a>
                                                @else
                                                    <a title="Add to wishlist" href="javascript:void();" class="open-login-form link-wishlist">&nbsp;</a>
                                                @endif
                                                <span class="qview">
										   <a class="vt_quickview_handler"
                                              data-original-title="Quick View"
                                              data-placement="left"
                                              data-toggle="tooltip"
                                              href="{{ route('frontend.show-detail-product', ['id' => $latestProduct->id, 'parentId' => $categorySelected->id]) }}">
                                               <span>Quick View</span>
                                           </a>
										   </span>
                                            </div>
                                        </div>
                                        <a title="Add to cart" class="btn-cart" href="#">&nbsp;</a>
                                    </div>
                                    <div class="pro-info">
                                        <div class="pro-inner">
                                            <div class="pro-title product-name">
                                                <a href="{{ route('frontend.show-detail-product', ['id' => $latestProduct->id, 'parentId' => $categorySelected->id]) }}">{{ $latestProduct->name }}</a>
                                            </div>
                                            <div class="pro-content">
                                                <div class="wrap-price">
                                                    <div class="price-box">
                                                         <span class="regular-price">
                                                             <span class="price">
                                                                 {{ number_format($latestProduct->price - $latestProduct->price * $latestProduct->sale / 100 ,2,",",".") }} VND
                                                             </span>
                                                         </span>
                                                    </div>
                                                </div>
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <div class="rating" style="width: {{ $latestProduct->average_star * 20 }}%"></div>
                                                    </div>
                                                    <span class="amount"><a href="javascript:void();">{{ $latestProduct->times_rating }} đánh giá</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end item wrap -->
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block box-banner">
        <div id="box-banner" class="carousel slide" data-ride="carousel" data-interval="1500"><ol class="carousel-indicators">
                <li class="active" data-target="#box-banner" data-slide-to="0"></li>
                <li data-target="#box-banner" data-slide-to="1"></li>
                <li data-target="#box-banner" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active"><a href="#"><img src="store/images/banner13.png" alt="banner" /></a>
                    <div class="std">
                        <span class="t1">up to 45%</span>
                        <span class="t2">Nokia Lumia 920</span>
                        <span class="t3">At verov eos et accusamus et iusto ods un dignissimos ducimus qui blan ditiis prasixer esentium</span>
                    </div>
                    <a class="gt-shop" href="#">Shop Now</a>
                </div>
                <div class="item"><a href="#"><img src="store/images/banner14.png" alt="banner" /></a>
                    <div class="std">
                        <span class="t1">up to 50%</span>
                        <span class="t2">Nokia Lumia 920</span>
                        <span class="t3">At verov eos et accusamus et iusto ods un dignissimos ducimus qui blan ditiis prasixer esentium</span>
                    </div>
                    <a class="gt-shop" href="#">Shop Now</a>

                </div>
                <div class="item"><a href="#"><img src="store/images/banner15.png" alt="banner" /></a>
                    <div class="std">
                        <span class="t1">up to 80%</span>
                        <span class="t2">Nokia Lumia 920</span>
                        <span class="t3">At verov eos et accusamus et iusto ods un dignissimos ducimus qui blan ditiis prasixer esentium</span>
                    </div>
                    <a class="gt-shop" href="#">Shop Now</a>

                </div>
            </div>
        </div>
    </div>
</div>