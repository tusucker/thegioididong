<div style="z-index: 100000" class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-title text-center">
                    <h4>Đăng ký tài khoản</h4>
                </div>

                <div class="d-flex flex-column text-center">
                    <form id="form-register" method="POST" action="{{ route('frontend.register') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="name"placeholder="Nhập tên của bạn">
                        </div>
                        <div style="display: flex;">
                            <span class="badge badge-danger message-error message-error-name" style="margin-bottom: 15px; color: red; background: white; display: none">
                            </span>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email"placeholder="Nhập email của bạn">
                        </div>
                        <div style="display: flex;">
                            <span class="badge badge-danger message-error message-error-email" style="margin-bottom: 15px; color: red; background: white; display: none">
                            </span>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu của bạn">
                        </div>
                        <div style="display: flex;">
                            <span class="badge badge-danger message-error message-error-password" style="margin-bottom: 15px; color: red; background: white; display: none">
                            </span>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="re_password" placeholder="Xác nhận mật khẩu">
                        </div>
                        <div style="display: flex;">
                            <span class="badge badge-danger message-error message-error-re-password" style="margin-bottom: 15px; color: red; background: white; display: none">
                            </span>
                        </div>
                        <button type="button" class="btn btn-info btn-block btn-round btn-register">Đăng ký</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>