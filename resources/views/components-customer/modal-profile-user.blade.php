<div style="z-index: 100000" class="modal fade" id="modalProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <input type="hidden" id="status-information" value="{{ \Session::get('status-information') ?? 0 }}">
            @php
                \Session::put('status-information', 0)
            @endphp
            <div class="modal-header border-bottom-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#information" aria-controls="home" role="tab" data-toggle="tab">Thông tin cá nhân</a></li>
                    <li role="presentation"><a href="#change-information" aria-controls="messages" role="tab" data-toggle="tab">Thay đổi thông tin cá nhân</a></li>
                    <li role="presentation"><a href="#change-password" aria-controls="messages" role="tab" data-toggle="tab">Thay đổi mật khẩu</a></li>
                    <li role="presentation"><a href="#history" aria-controls="messages" role="tab" data-toggle="tab">Lịch sử giao dịch</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    @if(\Auth::check())
                    <div role="tabpanel" class="tab-pane active" id="information" style="padding: 40px">
                        <table class="table table-borderless" style="color: black; font-weight: bolder">
                            <tbody>
                            <tr>
                                <td>Họ và tên</td>
                                <td>{{ \Auth::user()['name'] }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{ \Auth::user()['email'] }}</td>
                            </tr>
                            <tr>
                                <td>Ảnh đại diện</td>
                                <td><img src="{{ \Auth::user()['avatar'] != '' ? \Auth::user()['avatar'] : 'https://ipsumimage.appspot.com/300x280' }}" alt="" style="width: 100px"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="change-information" style="padding: 50px">
                        <form method="POST" action="{{ route('frontend.change-information') }}" id="form-change-information">
                            @csrf
                            <div class="form-group">
                                <label for="name">Họ và tên</label>
                                <input type="text" class="form-control" name="name" value="{{ \Auth::user()['name'] }}">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ \Auth::user()['email'] }}">
                            </div>
                            <div class="form-group">
                                <label for="phone">Ảnh đại diện</label>
                                <input type="file" class="form-control" id="avatar" name="avatar">
                            </div>
                            <div class="show-image p-3">
                                <img src="{{ \Auth::user()['avatar'] != '' ? \Auth::user()['avatar'] : 'https://ipsumimage.appspot.com/300x280' }}" alt="main-image" style="width: 140px">
                            </div>

                            <button style="font-size: 13px; font-weight: bolder; width: 100%; margin-top: 13px; background: #2ecc71; color: white; border: none" class="btn btn-change-information" type="button">Thay đổi</button>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="change-password" style="padding: 50px">
                        <form method="POST" action="{{ route('frontend.change-password') }}" id="form-change-password">
                            @csrf
                            <div class="form-group">
                                <label for="name">Nhập mật khẩu cũ</label>
                                <input type="password" class="form-control" name="old_password">
                            </div>
                            <div style="display: flex">
                                <span class="badge badge-danger message-error message-error-old-password" style="margin-bottom: 15px; color: red; background: white; display: none">
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="name">Nhập mật khẩu mới</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                            <div style="display: flex">
                                <span class="badge badge-danger message-error message-error-password" style="margin-bottom: 15px; color: red; background: white; display: none">
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="name">Nhập lại mật khẩu mới</label>
                                <input type="password" class="form-control" name="re_password">
                            </div>
                            <div style="display: flex">
                                <span class="badge badge-danger message-error message-error-re-password" style="margin-bottom: 15px; color: red; background: white; display: none">
                                </span>
                            </div>
                            <button style="font-size: 13px; font-weight: bolder; width: 100%; margin-top: 13px; background: #2ecc71; color: white; border: none" class="btn btn-change-password" type="button">Thay đổi</button>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="history" style="padding: 50px">
                        @if(Auth::user()->orders->count() > 0)
                            <table class="table table-hover" style="color: black">
                                <thead>
                                <tr>
                                    <th scope="col" style="text-align: center">STT</th>
                                    <th scope="col" style="text-align: center">Tổng tiền</th>
                                    <th scope="col" style="text-align: center">Trạng thái</th>
                                    <th scope="col" style="text-align: center">Giao tại</th>
                                    <th scope="col" style="text-align: center">Thời gian tạo</th>
                                    <th scope="col" style="text-align: center">Xem chi tiết</th>
                                    <th scope="col" style="text-align: center">Hủy</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Auth::user()->orders as $key => $order)
                                    <tr>
                                        <th style="text-align: center" scope="row">{{ $key + 1 }}</th>
                                        <td style="text-align: center">{{ number_format($order->total_money,2,',','.')  }} VND</td>
                                        <td style="text-align: center" class="status-order">
                                            @if($order->status == 1)
                                                <span class="badge badge-success" style="background: green">Tiếp nhận</span>
                                            @elseif($order->status == 2)
                                                <span class="badge badge-dark" style="background: gray">Đang vận chuyển</span>
                                            @elseif($order->status == 3)
                                                <span class="badge badge-info" style="background: yellow">Đã bàn giao</span>
                                            @else
                                                <span class="badge badge-danger" style="background: red">Đã hủy</span>
                                            @endif
                                        </td>
                                        <td style="text-align: center">{{ $order->address }}</td>
                                        <td style="text-align: center">{{ $order->created_at }}</td>
                                        <td style="text-align: center">
                                            <button class="btn btn-success btn-detail-order"
                                                    data-action="{{ route('frontend.show-detail-order', ['id' => $order->id]) }}">
                                                <i class="fas fa-eye"></i>
                                            </button></td>
                                        <td style="text-align: center">
                                            @if($order->status != 4)
                                                <button class="btn btn-danger btn-cancel-order"
                                                        data-action="{{ route('frontend.cancel-order', ['id' => $order->id]) }}">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div style="text-align: center;
                                                font-size: 30px;
                                                font-weight: bold;
                                                color: black;
                                                padding: 45px;">Chưa có đơn hàng nào</div>
                        @endif
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>