<div style="z-index: 100000" class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-title text-center">
                    <h4>Đăng nhập</h4>
                </div>
                <div style="display: flex; align-items: center;justify-content: center;">
                    <span class="badge badge-danger message-error" style="margin-bottom: 15px; padding: 10px; color: red; background: white;  display: none">
                        Email hoặc mật khẩu không chính xác
                    </span>
                </div>

                <div class="d-flex flex-column text-center">
                    <form id="form-login" method="POST" action="{{ route('frontend.login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control" name="email"placeholder="Nhập email của bạn">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu của bạn">
                        </div>
                        <button type="button" class="btn btn-info btn-block btn-round btn-login">Đăng nhập</button>
                    </form>

{{--                    <div class="text-center text-muted delimiter">or use a social network</div>--}}
{{--                    <div class="d-flex justify-content-center social-buttons">--}}
{{--                        <button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Twitter">--}}
{{--                            <i class="fab fa-twitter"></i>--}}
{{--                        </button>--}}
{{--                        <button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Facebook">--}}
{{--                            <i class="fab fa-facebook"></i>--}}
{{--                        </button>--}}
{{--                        <button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Linkedin">--}}
{{--                            <i class="fab fa-linkedin"></i>--}}
{{--                        </button>--}}
{{--                        </di>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>