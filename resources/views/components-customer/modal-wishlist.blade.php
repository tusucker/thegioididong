<div style="z-index: 100000" class="modal fade" id="modalWishList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="vt_megamenu_content">
                    <div class="mega-menu-02">
                        <h1 class="title" style="margin: 0 165px 50px 165px; color: black;">SẢN PHẨM YÊU THÍCH CỦA BẠN</h1>
                        <div class="block row vt-slider vt-slider67">
                            <div class="slider-inner">
                                <div class="container-slider">
                                    <div class="products-grid row">
                                        @if(count($listProduct) > 0)
                                            @foreach($listProduct as $product)
                                                <div class="item col-md-4">
                                                    <div class="remove-wishlist-item" data-action="{{ route('frontend.remove-from-wishlist', ['id' => $product->id]) }}"><i class="fas fa-backspace"></i></div>
                                                    <div class="item-wrap">
                                                        <div class="item-image">
                                                            <a class="product-image no-touch" href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}" title="{{ $product->name }}">
                                                                <img class="first_image" style="width: 240px; height: 240px" src="{{ $product->avatar }}" alt="Product demo"/>
                                                            </a>
                                                        </div>
                                                        <div class="pro-info">
                                                            <div class="pro-inner">
                                                                <div class="pro-title product-name">
                                                                    <a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">{{ $product->name }}</a>
                                                                </div>
                                                                <div class="add-to-box add-to-cart">
                                                                    <div class="add-to-cart">
                                                                        <div class="add-to-cart-buttons">
                                                                            <a href="{{ route('frontend.add-to-cart-simple',
                                                                                    ['productId' => $product->id]) }}"
                                                                               title="Add to Cart" class="button btn-cart"
                                                                               style="padding: 5px; background: yellow;
                                                                           color: red; border-radius: 5px; font-weight: bold;"
                                                                            >
                                                                                Thêm vào giỏ hàng
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="pro-content">
                                                                    <div class="wrap-price">
                                                                        <div class="price-box">
                                                                         <span class="regular-price" style="margin: 7px 0;">
                                                                             <span class="price">
                                                                                 {{ number_format($product->price,2,",",".") . ' VND' }}
                                                                             </span>
                                                                         </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="ratings">
                                                                        <div class="rating-box">
                                                                            <div class="rating" style="width: {{ $product->average_star * 20 }}%"></div>
                                                                        </div>
                                                                        <span class="amount"><a href="javascript:void();">{{ $product->times_rating }} đánh giá</a></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end item wrap -->
                                                </div>
                                            @endforeach
                                        @else
                                            <div style="text-align: center;
                                                font-size: 30px;
                                                font-weight: bold;
                                                color: black;
                                                padding: 45px;">Chưa có sản phẩm nào</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>