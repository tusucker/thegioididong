<div style="z-index: 100001" class="modal fade" id="modalDetailOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover" style="color: black">
                    <thead>
                    <tr>
                        <th scope="col" style="text-align: center">STT</th>
                        <th scope="col" style="text-align: center">Tên sản phẩm</th>
                        <th scope="col" style="text-align: center">Ảnh</th>
                        <th scope="col" style="text-align: center">Số lượng</th>
                        <th scope="col" style="text-align: center">Đơn giá</th>
                        <th scope="col" style="text-align: center">Thành tiền</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listDetailOrder as $key => $detailOrder)
                        <tr>
                            <th scope="row" style="text-align: center">{{ $key + 1 }}</th>
                            <td style="text-align: center">{{ $detailOrder->product->name }}</td>
                            <td style="text-align: center"><img style="width: 100px" src="{{ $detailOrder->product->avatar }}" alt=""></td>
                            <td style="text-align: center">{{ $detailOrder->quantity }}</td>
                            <td style="text-align: center">{{ number_format($detailOrder->price - $detailOrder->price * $detailOrder->sale / 100,2,',','.') }} VND</td>
                            <td style="text-align: center">{{ number_format(($detailOrder->price - $detailOrder->price * $detailOrder->sale / 100) * $detailOrder->quantity,2,',','.') }} VND</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>