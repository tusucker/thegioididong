<div class="col-md-4">
    <!-- Search Widget -->
    <div class="card my-4">
        <h5 class="card-header">Search</h5>
        <div class="card-body">
            <div class="input-group">
                <form method="GET" action="{{ route('frontend.list-article') }}">
                    @csrf
                    <input style="width: 200px; margin-right: 20px;" type="text" class="form-control" name="text_search" placeholder="Search for...">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">Go!</button>
                    </span>
                </form>
            </div>
        </div>
    </div>
    <h2 style="color: #ff8300; margin-top: 40px">Latest Review</h2>
    @foreach($listLatestReviewArticle as $article)
        <div class="row card blog-simple">
            <div class="card-body">
                <div class="col-md-6">
                    <img class="card-img-top" src="{{ $article->avatar }}" alt="Card image cap">
                </div>
                <div class="col-md-6">
                    <div class="card-title" style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                        <a href="{{ route('frontend.detail-article', ['id' => $article->id]) }}">{{ $article->name }}</a>
                    </div>
                    <div>
                        Posted on <span style="color: black">{{ $article->created_at }}</span> by <span style="color: black; font-weight: bolder">{{ $article->user->name }}</span>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <h2 style="color: #ff8300; margin-top: 40px">Sản phẩm bán chạy nhất</h2>
    @foreach($listSellerProduct as $key => $product)
        @if($key < 4)
            <div class="row card blog-simple">
                <div class="card-body">
                    <div class="col-md-6">
                        <img class="card-img-top" src="{{ $product->avatar }}" alt="Card image cap">
                    </div>
                    <div class="col-md-6">
                        <div class="card-title" style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                            <a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">{{ $product->name }}</a>
                        </div>
                        <div>
                            {{ number_format($product->price, 2, ',', '.') }} VND
                        </div>
                        <div class="ratings">
                            <div class="rating-box">
                                <div class="rating" style="width: {{ $product->average_star * 20 }}%"></div>
                            </div>
                            <span class="amount"><a href="javascript:void();">{{ $product->times_rating }} đánh giá</a></span>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
</div>