<div class="position-02">
    <div class="container">
        <div class="row">
            <div class="title-sp">
                SẢN PHẨM MỚI NHẤT
                <div class="std">
                    Nhận các sản phẩm cửa hàng trực tuyến độc quyền.
                </div>
            </div>
            <div class="block vt-slider vt-slider3">
                <div class="slider-inner">
                    <div class="container-slider">
                        <div class="products-grid">
                            @foreach($listNewestProducts as $product)
                                <div class="item">
                                    <div class="item-wrap">
                                        <div class="item-image">
                                            <a class="product-image no-touch" href="#" title="{{ $product->name }}">
                                                <img class="first_image" style="width: 186px; height: 186px" src="{{ $product->avatar }}"
                                                     alt="Product demo"/>
                                            </a>
                                            <div class="item-btn">
                                                <div class="box-inner">
                                                    @if(\Auth::check())
                                                        <a title="Add to wishlist" href="{{ route('frontend.add-to-wishlist', ['id' => $product->id]) }}"
                                                           class="link-wishlist"
                                                           style="background: {{ $listWishList->contains($product->id) ? 'yellow' : ''}} ">&nbsp;</a>
                                                    @else
                                                        <a title="Add to wishlist" href="javascript:void();" class="open-login-form link-wishlist">&nbsp;</a>
                                                    @endif
                                                    <span class="qview">
                                                        <a class="vt_quickview_handler"
                                                           data-original-title="Quick View"
                                                           data-placement="left"
                                                           data-toggle="tooltip"
                                                           href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">
                                                            <span>Quick View</span>
                                                        </a>
										            </span>
                                                </div>
                                            </div>
                                            <a title="Add to cart" class="btn-cart" href="{{ route('frontend.add-to-cart-simple', ['productId' => $product->id]) }}">&nbsp;</a>
                                        </div>
                                        <div class="pro-info">
                                            <div class="pro-inner">
                                                <div class="pro-title product-name">
                                                    <a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">{{ $product->name }}</a></div>
                                                <div class="pro-content">
                                                    <div class="wrap-price">
                                                        <div class="price-box">
                                                            <span class="regular-price">
                                                                <span class="price">
                                                                    {{ number_format($product->price - $product->price * $product->sale / 100 ,2,",",".") }} VND
                                                                </span>
                                                            </span>
                                                            <p class="special-price" style=" visibility: {{ $product->sale == 0 ? 'hidden' : 'visible' }}">
                                                                <span class="price">{{ number_format($product->price,2,",",".") }} VND</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <div class="rating" style="width: {{ $product->average_star * 20 }}%"></div>
                                                        </div>
                                                        <span class="amount"><a href="javascript:void();">{{ $product->times_rating }} đánh giá</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end item wrap -->
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="navslider">
                        <a class="prev" href="#">Prev</a>
                        <a class="next" href="#">Next</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>