<div class="position-09">
    <div class="container">
        <div class="row">
            <div class="popular-cate">
                <div class="title-cate">
                    Popular Categories
                </div>
                <div class="content">
                    <div class="item first">
                        <div class="title">
                            <a href="{{ route('frontend.show-categories', ['slug' => $categoryPhone->slug, 'id' => $categoryPhone->id] ) }}">Smart phone</a>
                        </div>
                        <div class="image">
                            <a href="{{ route('frontend.show-categories', ['slug' => $categoryPhone->slug, 'id' => $categoryPhone->id] ) }}">
                                <img src="store/images/catalog/cate1.png" alt=""/>
                            </a>
                        </div>
                        <div class="tag">
                            @foreach($categoryPhone->categoryChildren as $category)
                                <a href="javascript:void();">{{ $category->name }}</a>,
                            @endforeach
                        </div>
                        <a class="view-all" href="{{ route('frontend.show-categories', ['slug' => $categoryPhone->slug, 'id' => $categoryPhone->id] ) }}">View all</a>
                    </div>
                    <div class="item">
                        <div class="title">
                            <a href="{{ route('frontend.show-categories', ['slug' => $categoryLaptop->slug, 'id' => $categoryLaptop->id] ) }}">
                                laptops
                            </a>
                        </div>
                        <div class="image">
                            <a href="{{ route('frontend.show-categories', ['slug' => $categoryLaptop->slug, 'id' => $categoryLaptop->id] ) }}">
                                <img src="store/images/catalog/cate3.png" alt=""/>
                            </a>
                        </div>
                        <div class="tag">
                            @foreach($categoryLaptop->categoryChildren as $category)
                                <a href="javascript:void();">{{ $category->name }}</a>,
                            @endforeach
                        </div>
                        <a class="view-all" href="{{ route('frontend.show-categories', ['slug' => $categoryLaptop->slug, 'id' => $categoryLaptop->id] ) }}">View all</a>
                    </div>
                    <div class="item">
                        <div class="title">
                            <a href="{{ route('frontend.show-categories', ['slug' => $categoryAccessory->slug, 'id' => $categoryAccessory->id] ) }}">
                                Accessories
                            </a>
                        </div>
                        <div class="image">
                            <a href="{{ route('frontend.show-categories', ['slug' => $categoryAccessory->slug, 'id' => $categoryAccessory->id] ) }}">
                                <img src="store/images/catalog/cate5.png" alt=""/>
                            </a>
                        </div>
                        <div class="tag">
                            @foreach($categoryAccessory->categoryChildren as $category)
                                <a href="javascript:void();">{{ $category->name }}</a>,
                            @endforeach
                        </div>
                        <a class="view-all" href="{{ route('frontend.show-categories', ['slug' => $categoryAccessory->slug, 'id' => $categoryAccessory->id] ) }}">View all</a>
                    </div>
                    <div class="item last">
                        <div class="title">
                            <a href="{{ route('frontend.show-categories', ['slug' => $categoryWatch->slug, 'id' => $categoryWatch->id] ) }}">Watch</a>
                        </div>
                        <div class="image">
                            <a href="{{ route('frontend.show-categories', ['slug' => $categoryWatch->slug, 'id' => $categoryWatch->id] ) }}">
                                <img src="store/images/catalog/cate6.png" alt=""/>
                            </a>
                        </div>
                        <div class="tag">
                            @foreach($categoryWatch->categoryChildren as $category)
                                <a href="javascript:void();">{{ $category->name }}</a>,
                            @endforeach
                        </div>
                        <a class="view-all" href="{{ route('frontend.show-categories', ['slug' => $categoryWatch->slug, 'id' => $categoryWatch->id] ) }}">View all</a>
                    </div>
                    <div class="position-01">
                        <div class="img01"><a href="#"><img src="store/images/position-01/images-01.png" alt=""/></a>
                        </div>
                        <div class="img02"><a href="#"><img src="store/images/position-01/images-02.png" alt=""/></a>
                        </div>
                        <div class="img03"><a href="#"><img src="store/images/position-01/images-03.png" alt=""/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
