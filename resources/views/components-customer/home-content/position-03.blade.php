<div class="position-03">
    <div class="container">
        <div class="row">
            <div class="box-1">
                <h2>featured</h2>
                <ul class="control">
                    <li data-rel="0px" class="ct1 active">Điện thoại</li>
                    <li data-rel="-852px" class="ct2">Máy tính</li>
                    <li data-rel="-1704px" class="ct3">Phụ kiện</li>
                    <li data-rel="-2556px" class="ct4">Đồng hồ</li>
                </ul>
            </div>
            <div class="box-2">
                <div class="slide">
                    @foreach($listMixProduct as $product)
                        <div class="item">
                            <div class="box-image">
                                <a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}"><img style="width: 418px; height: 418px" src="{{ $product->avatar }}" alt=""/></a>
                            </div>
                            <div class="product-shop">
                                <div class="pro-info">
                                    <div class="pro-inner">
                                        <div class="pro-title product-name">
                                            <a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">{{ $product->name }}</a>
                                        </div>
                                        <div class="pro-content">
                                            <div class="wrap-price">
                                                <div class="price-box">
                                                     <span class="regular-price">
                                                        <span class="price">{{ number_format($product->price - $product->price * $product->sale / 100 ,2,",",".") }} VND</span>
                                                     </span>
                                                    <p class="special-price" style=" visibility: {{ $product->sale == 0 ? 'hidden' : 'visible' }}">
                                                        <span class="price">{{ number_format($product->price,2,",",".") }} VND</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width: {{ $product->average_star * 20 }}%"></div>
                                                </div>
                                                <span class="amount"><a href="javascript:void();">{{ $product->times_rating }} đánh giá</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="std">
                                    {!! $product->description !!}
                                </div>
                                <div class="item-btn">
                                    <div class="box-inner">
                                        @if(\Auth::check())
                                            <a title="Add to wishlist" href="{{ route('frontend.add-to-wishlist', ['id' => $product->id]) }}"
                                               class="link-wishlist" style="background: {{ $listWishList->contains($product->id) ? 'yellow' : ''}} ">&nbsp;</a>
                                        @else
                                            <a title="Add to wishlist" href="javascript:void();" class="open-login-form link-wishlist">&nbsp;</a>
                                        @endif
                                        <span class="qview">
							<a class="vt_quickview_handler"
                               data-original-title="Quick View"
                               data-placement="left"
                               data-toggle="tooltip"
                               href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">
                                <span>Quick View</span>
                            </a>
						   </span>
                                        <a title="Add to cart" class="btn-cart" href="{{ route('frontend.add-to-cart-simple', ['productId' => $product->id]) }}">Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>