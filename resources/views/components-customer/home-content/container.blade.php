<div class="container">
    <div class="row">
        <div class="slide-show">
            <div class="vt-slideshow">
                <ul>
                    @foreach($listRandomBanner as $key => $banner)
                        <li class="slide{{ $key + 1 }}" data-transition="random"><img src="{{ $banner->path }}" alt=""/></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>