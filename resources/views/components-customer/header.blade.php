<div id="box-header">
    <div class="header-container">
        <div class="header">
            <div class="box-header-01" style="margin-bottom: 15px">
                <div class="container">
                    <div class="row">
                        <div class="logo" style="margin-top: 0 !important;">
                            <a href="{{ route('frontend.index') }}">
                                <img style="width: 110px; height: 75px;" src="store/images/T_H.jpg" alt=""/>
                            </a>
                        </div>
                        <div class="menu">
                            <div class="box-main-menu">
                                <div class="main-menu">
                                    <ul style="font-weight: bolder;">
                                        <li class="item1 first"><a href="{{ route('frontend.index') }}">Trang chủ</a></li>
                                        <li class="item2 megamenu-parent">
                                            <a href="{{ route('frontend.show-categories',
                                                                            [
                                                                                'slug' => $categoryPhone->slug,
                                                                                'id' => $categoryPhone->id
                                                                            ]
                                                                            ) }}"
                                            >
                                                Sản phẩm
                                            </a>
                                            <div class="vt_megamenu_content">
                                                <div class="mega-menu-01">
                                                    <div class="menu-01 menu-01-cate">
                                                        <div class="title">
                                                            <span> Loại sản phẩm </span>
                                                        </div>
                                                        <ul class="content-col">
                                                            <li class="first">
                                                                <a class="mega-lap"
                                                                   href="{{ route('frontend.show-categories',
                                                                            [
                                                                                'slug' => $categoryLaptop->slug,
                                                                                'id' => $categoryLaptop->id
                                                                            ]
                                                                            ) }}">
                                                                    <span>{{ $categoryLaptop->name }}</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="mega-mob"
                                                                   href="{{ route('frontend.show-categories',
                                                                            [
                                                                                'slug' => $categoryPhone->slug,
                                                                                'id' => $categoryPhone->id
                                                                            ]
                                                                            ) }}">
                                                                    <span>{{ $categoryPhone->name }}</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="mega-cam"
                                                                   href="{{ route('frontend.show-categories',
                                                                            [
                                                                                'slug' => $categoryAccessory->slug,
                                                                                'id' => $categoryAccessory->id
                                                                            ]
                                                                            ) }}">
                                                                    <span>{{ $categoryAccessory->name }}</span>
                                                                </a>
                                                            </li>
                                                            <li class="last">
                                                                <a class="mega-rad"
                                                                   href="{{ route('frontend.show-categories',
                                                                            [
                                                                                'slug' => $categoryWatch->slug,
                                                                                'id' => $categoryWatch->id
                                                                            ]
                                                                            ) }}">
                                                                    <span>{{ $categoryWatch->name }}</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="menu-01 menu-01-top">
                                                        <div class="title">
                                                            <span>Tops</span>
                                                            <span class="hot">Hot</span>
                                                        </div>
                                                        <ul class="content-col">
                                                            <li class="first">
                                                                <a href="{{ route('frontend.show-categories',
                                                                            [
                                                                                'slug' => $categoryPhone->slug,
                                                                                'id' => $categoryPhone->id
                                                                            ]
                                                                            ) }}">
                                                                    <span>{{ $categoryPhone->name }}</span>
                                                                </a>
                                                            </li>
                                                            <li class="last">
                                                                <a href="{{ route('frontend.show-categories',
                                                                            [
                                                                                'slug' => $categoryWatch->slug,
                                                                                'id' => $categoryWatch->id
                                                                            ]
                                                                            ) }}">
                                                                    <span>{{ $categoryWatch->name }}</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div id="feature" class="menu-01 menu-01-feature carousel-feature">
                                                        <div class="title">
                                                            <span>Featured</span>
                                                            <span class="new">New</span>
                                                        </div>
                                                        <div class="carousel-inner">
                                                            <div class="item active">
                                                                <div class="image">
                                                                    <img style="width: 270px; height: 181px" alt="" src="{{ $featureProduct->avatar }}"/>
                                                                </div>
                                                                <div class="carousel-caption-feature">
                                                                    <div class="title-advan">
                                                                        <span class="name-advan">{!! $featureProduct->name !!}</span>
                                                                        <div class="text-des-advan">
                                                                            <span>{!! $featureProduct->description !!}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="menu-01 menu-01-sale">
                                                        <div class="title"><span>Sale</span></div>
                                                        <div class="content">
                                                            <img style="width: 270px; height: 181px" alt="" src="{{ $randomSaleProduct->avatar }}"/>
                                                            <p class="description">{!! $randomSaleProduct->description !!}</p>
                                                            <div class="shop-now"><a title="Shop now" href="#"><span>Shop now</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="item3 megamenu-parent"><a href="javascript:void();">sản phẩm nổi bật</a>
                                            <div class="vt_megamenu_content">
                                                <div class="mega-menu-02">
                                                    <h1 class="title">SẢN PHẨM ĐƯỢC ƯA CHUỘNG NHẤT</h1>
                                                    <div class="block row vt-slider vt-slider67">
                                                        <div class="slider-inner">
                                                            <div class="container-slider">
                                                                <div class="products-grid">
                                                                    @foreach($listSellerProduct as $product)
                                                                        <div class="item">
                                                                            <div class="item-wrap">
                                                                                <div class="item-image">
                                                                                    <a class="product-image no-touch"
                                                                                       href="{{ route('frontend.show-detail-product',
                                                                                                ['id' => $product->id, 'parentId' => $product->category_id]) }}"
                                                                                       title="{{ $product->name }}">
                                                                                        <img class="first_image" style="width: 240px; height: 240px" src="{{ $product->avatar }}" alt="Product demo"/>
                                                                                    </a>
                                                                                    <div class="item-btn">
                                                                                        <div class="box-inner">
                                                                                            @if(\Auth::check())
                                                                                                <a title="Add to wishlist"
                                                                                                   href="{{ route('frontend.add-to-wishlist', ['id' => $product->id]) }}"
                                                                                                   class="link-wishlist" style="background: {{ $listWishList->contains($product->id) ? 'yellow' : ''}} " >&nbsp;</a>
                                                                                            @else
                                                                                                <a title="Add to wishlist" href="javascript:void();" class="open-login-form link-wishlist">&nbsp;</a>
                                                                                            @endif
                                                                                            <span class="qview">
                                                                                                <a class="vt_quickview_handler"
                                                                                                   data-original-title="Quick View"
                                                                                                   data-placement="left"
                                                                                                   data-toggle="tooltip"
                                                                                                   href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">
                                                                                                    <span>Quick View</span>
                                                                                                </a>
							                                                                </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <a title="Add to cart" class="btn-cart"
                                                                                       href="{{ route('frontend.add-to-cart-simple', ['productId' => $product->id]) }}">&nbsp;</a>
                                                                                </div>
                                                                                <div class="pro-info">
                                                                                    <div class="pro-inner">
                                                                                        <div class="pro-title product-name">
                                                                                            <a href="{{ route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]) }}">{{ $product->name }}</a>
                                                                                        </div>
                                                                                        <div class="pro-content">
                                                                                            <div class="wrap-price">
                                                                                                <div class="price-box">
                                                                                                     <span class="regular-price">
                                                                                                         <span class="price">
                                                                                                             {{ number_format($product->price,2,",",".") . ' VND' }}
                                                                                                         </span>
                                                                                                     </span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="ratings">
                                                                                                <div class="rating-box">
                                                                                                    <div class="rating" style="width: {{ $product->average_star * 20 }}%"></div>
                                                                                                </div>
                                                                                                <span class="amount"><a href="javascript:void();">{{ $product->times_rating }} đánh giá</a></span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end item wrap -->
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="item4"><a href="{{ route('frontend.list-article') }}">bài viết </a></li>
                                        <li class="item5"><a href="javascript:void();">liên hệ </a></li>
                                        <li class="item6" style="display: {{ isset($status) ? 'none' : 'block' }}">
                                            <i class="fa fa-search" aria-hidden="true" style="color: white; margin-right: 5px"></i>
                                            <a href="javascript:void();">
                                                <input style="height: 30px; padding: 6px; color: black" id="live-search" type="text" placeholder="Nhập thông tin tìm kiếm ..." data-action="{{ route('frontend.search') }}">
                                            </a>
                                        </li>
                                        <div class="show-result-search" style="display: none">
                                        </div>
                                        @if (! Auth::check())
                                            <li><a class="open-login-form" href="#">Đăng nhập </a></li>
                                            <li><a class="open-register-form" href="#">Đăng ký </a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                        <div class="account-and-cart" style="margin-top: {{! Auth::check() ? '-75px' : ''}}">
                            @if (Auth::check())
                                <div class="my-account">
                                    <div class="content">
                                        <ul class="left" style="width: 210px !important;">
                                            <li><a class="top-link-myaccount" href="#">Thông tin người dùng</a></li>
                                            <li><a class="top-link-wishlist my-wishlist" data-action="{{ route('frontend.list-wishlist') }}" href="javascript:void();">Sản phẩm yêu thích</a></li>
                                            <li><a class="top-link-login" href="{{ route('frontend.logout') }}">Đăng xuất</a></li>
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            <div class="box-cart">
                                <div class="cart-mini">
                                    <a class="title" href="{{ route('frontend.show-cart-detail') }}">
                                        <span class="item" style="margin: 17px">{{ Cart::count() }}</span>
                                    </a>
                                    <div class="block-content" style="visibility: hidden">
                                        <div class="inner">
                                            <p class="block-subtitle">Recently added item(s)</p>
                                            <ol id="cart-sidebar" class="mini-products-list">
                                                <li class="item">
                                                    <a href="#" title="Fashion Product 09" class="product-image">
                                                        <img src="store/images/product/small/image-demo-1.jpg"
                                                             alt="Fashion Product 09"/>
                                                    </a>
                                                    <a href="#" class="btn-remove">Remove This Item</a>
                                                    <a href="#" title="Edit item" class="btn-edit">Edit item</a>
                                                    <div class="product-details">
                                                        <p class="product-name"><a title="Fashion Product 09" href="#">Fashion
                                                                Product 09</a></p>
                                                        <span class="price">$200.00</span>
                                                        <div class="qty-abc">
                                                            <a title="Decrement" class="qty-change-left"
                                                               href="#">down</a>
                                                            <input class="input-text qty" type="text" value="1"/>
                                                            <a title="Increment" class="qty-change-right"
                                                               href="#">up</a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                            <div class="summary">
                                                <p class="subtotal">
                                                    <span class="label">Subtotal:</span> <span
                                                            class="price">$200.00</span>
                                                </p>
                                            </div>
                                            <div class="actions">
                                                <div class="a-inner">
                                                    <a class="btn-mycart" href="#" title="View my cart">view my cart</a>
                                                    <a href="" title="Checkout" class="btn-checkout">Checkout</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bgr-menu">
                </div>
            </div>
        </div>
    </div>
</div>