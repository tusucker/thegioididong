<?php

namespace App\Traits;

trait DataTree
{
    public function showDataTree($data, $parent_id = 0, $level = 0)
    {
        $result = [];
        foreach ($data as $item) {
            if ($item['parent_id'] === $parent_id) {
                $item['level'] = $level;
                $result[] = $item;
                $child = $this->showDataTree($data, $item['id'], $level + 1);
                $result = array_merge($result, $child);
            }
        }

        return $result;
    }
}
