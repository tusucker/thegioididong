<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view-dashboard','App\Policies\DashboardPolicy@view');
        $this->defineGateCategory();
        $this->defineGateProduct();
        $this->defineGateArticle();
        $this->defineGateUser();
        $this->defineGateRole();
    }

    public function defineGateCategory()
    {
        Gate::define('view-category','App\Policies\CategoryPolicy@view');
        Gate::define('add-category','App\Policies\CategoryPolicy@add');
        Gate::define('edit-category','App\Policies\CategoryPolicy@edit');
        Gate::define('delete-category','App\Policies\CategoryPolicy@delete');
    }

    public function defineGateProduct()
    {
        Gate::define('view-product','App\Policies\ProductPolicy@view');
        Gate::define('add-product','App\Policies\ProductPolicy@add');
        Gate::define('edit-product','App\Policies\ProductPolicy@edit');
        Gate::define('delete-product','App\Policies\ProductPolicy@delete');
    }

    public function defineGateArticle()
    {
        Gate::define('view-article','App\Policies\ArticlePolicy@view');
        Gate::define('add-article','App\Policies\ArticlePolicy@add');
        Gate::define('edit-article','App\Policies\ArticlePolicy@edit');
        Gate::define('delete-article','App\Policies\ArticlePolicy@delete');
    }

    public function defineGateUser()
    {
        Gate::define('view-user','App\Policies\UserPolicy@view');
        Gate::define('add-user','App\Policies\UserPolicy@add');
        Gate::define('edit-user','App\Policies\UserPolicy@edit');
        Gate::define('delete-user','App\Policies\UserPolicy@delete');
    }

    public function defineGateRole()
    {
        Gate::define('view-role','App\Policies\RolePolicy@view');
        Gate::define('add-role','App\Policies\RolePolicy@add');
        Gate::define('edit-role','App\Policies\RolePolicy@edit');
        Gate::define('delete-role','App\Policies\RolePolicy@delete');
    }
}
