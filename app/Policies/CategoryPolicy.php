<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user)
    {
        return $user->checkPermissionAccess('view_category');
    }

    public function add(User $user)
    {
        return $user->checkPermissionAccess('add_category');
    }

    public function edit(User $user)
    {
        return $user->checkPermissionAccess('edit_category');
    }

    public function delete(User $user)
    {
        return $user->checkPermissionAccess('delete_category');
    }
}
