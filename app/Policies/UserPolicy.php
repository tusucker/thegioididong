<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user)
    {
        return $user->checkPermissionAccess('view_user');
    }

    public function add(User $user)
    {
        return $user->checkPermissionAccess('add_user');
    }

    public function edit(User $user)
    {
        return $user->checkPermissionAccess('edit_user');
    }

    public function delete(User $user)
    {
        return $user->checkPermissionAccess('delete_user');
    }
}
