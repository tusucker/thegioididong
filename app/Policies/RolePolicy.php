<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user)
    {
        return $user->checkPermissionAccess('view_role');
    }

    public function add(User $user)
    {
        return $user->checkPermissionAccess('add_role');
    }

    public function edit(User $user)
    {
        return $user->checkPermissionAccess('edit_role');
    }

    public function delete(User $user)
    {
        return $user->checkPermissionAccess('delete_role');
    }
}
