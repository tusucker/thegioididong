<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user)
    {
        return $user->checkPermissionAccess('view_article');
    }

    public function add(User $user)
    {
        return $user->checkPermissionAccess('add_article');
    }

    public function edit(User $user)
    {
        return $user->checkPermissionAccess('edit_article');
    }

    public function delete(User $user)
    {
        return $user->checkPermissionAccess('delete_article');
    }
}
