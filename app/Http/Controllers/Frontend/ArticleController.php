<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleComment;
use App\Models\Category;
use App\Models\Product;
use App\Models\WishList;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ArticleController extends Controller
{
    private $article;
    private $category;
    private $product;
    private $articleComment;
    private $wishList;

    public function __construct
    (
        Article $article,
        Category $category,
        Product $product,
        ArticleComment $articleComment,
        WishList $wishList
    )
    {
        $this->article = $article;
        $this->category = $category;
        $this->product = $product;
        $this->articleComment = $articleComment;
        $this->wishList = $wishList;
    }

    public function listArticle(Request $request)
    {
        $categoryPhone = $this->category->findOrFail(1);
        $categoryLaptop = $this->category->findOrFail(2);
        $categoryAccessory = $this->category->findOrFail(3);
        $categoryWatch = $this->category->findOrFail(4);
        $featureProduct = $this->product->where('active', 1)->orderBy('created_at','desc')->first();
        $randomSaleProduct = $this->product->where('active', 1)->where('sale', '!=', 0)->with('images')->get()->random();
        $listSellerProduct = $this->product->with('images')
            ->where('active', 1)->orderBy('pay','desc')
            ->get()->take(9);

        $listArticle = $this->article->orderBy('created_at', 'desc')->paginate(10);

        if($request->has('text_search')) {
            $listArticle = $this->article->where('name', 'like', '%'. $request->text_search. '%')->orderBy('created_at', 'desc')->paginate(10);
        }

        $listLatestReviewArticle = $this->article->orderBy('created_at', 'desc')->get()->take(4);
        $listWishList = $this->wishList->where('user_id', Auth::id())->get()->pluck('product_id');

        return view('customer.list-article', compact(
            'categoryPhone',
            'categoryLaptop',
            'categoryAccessory',
            'categoryWatch',
            'featureProduct',
            'randomSaleProduct',
            'listSellerProduct',
            'listArticle',
            'listLatestReviewArticle',
            'listWishList'
        ));
    }

    public function showDetailArticle($id)
    {
        $categoryPhone = $this->category->findOrFail(1);
        $categoryLaptop = $this->category->findOrFail(2);
        $categoryAccessory = $this->category->findOrFail(3);
        $categoryWatch = $this->category->findOrFail(4);
        $featureProduct = $this->product->where('active', 1)->orderBy('created_at','desc')->first();
        $randomSaleProduct = $this->product->where('active', 1)->where('sale', '!=', 0)->with('images')->get()->random();
        $listSellerProduct = $this->product->with('images')
            ->where('active', 1)->orderBy('pay','desc')
            ->get()->take(9);
        $listLatestReviewArticle = $this->article->orderBy('created_at', 'desc')->get()->take(4);

        $article = $this->article->with('articleComments')->findOrFail($id);

        $listArticleComment = $this->articleComment->with('articleCommentChildren')->where('article_id', $id)->orderBy('created_at', 'desc')->get();

        Carbon::setLocale('vi');
        $now = Carbon::now();

        foreach ($listArticleComment as $comment) {
            $comment['time'] = $comment->created_at->diffForHumans($now);
        }

        // increment view
        $article_key = 'article'.$id;
        if(!Session::has($article_key)){
            $this->article->where('id',$id)->increment('view');
            Session::put($article_key,1);
        }

        $listWishList = $this->wishList->where('user_id', Auth::id())->get()->pluck('product_id');

        return view('customer.detail-article', compact(
            'categoryPhone',
            'categoryLaptop',
            'categoryAccessory',
            'categoryWatch',
            'featureProduct',
            'randomSaleProduct',
            'listSellerProduct',
            'listLatestReviewArticle',
            'article',
            'listArticleComment',
            'listWishList'
        ));
    }

    public function sendComment($id, $parentId,  Request $request)
    {
        $dataInsert = $request->all();
        $dataInsert['article_id'] = $id;
        $dataInsert['user_id'] = Auth::id();
        $dataInsert['parent_id'] = $parentId;
        $articleComment = $this->articleComment->create($dataInsert);

        return response()->json([
            'message' => 'success',
            'data' => $articleComment
        ], 200);
    }
}
