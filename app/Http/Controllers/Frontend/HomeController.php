<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\RegisterAccountRequest;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Rating;
use App\Models\WishList;
use App\Traits\StorageImage;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Session;

class HomeController extends Controller
{
    use StorageImage;

    private $category;
    private $banner;
    private $product;
    private $article;
    private $user;
    private $rating;
    private $wishList;
    private $orderDetail;
    private $order;

    public function __construct
    (
        Category $category,
        Banner $banner,
        Product $product,
        Article $article,
        User $user,
        Rating $rating,
        WishList $wishList,
        OrderDetail $orderDetail,
        Order $order
    )
    {
        $this->category = $category;
        $this->banner = $banner;
        $this->product = $product;
        $this->article = $article;
        $this->user = $user;
        $this->rating = $rating;
        $this->wishList = $wishList;
        $this->orderDetail = $orderDetail;
        $this->order = $order;
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $status = 1;
        }
        else {
            $status = 2;
        }

        return response()->json([
            'status' => $status
        ], 200);
    }

    public function register(RegisterAccountRequest $request)
    {
        $dataInsert = $request->except('_token', 'password', 're_password');
        $dataInsert['password'] = Hash::make($request->password);
        $this->user->create($dataInsert);

        return response()->json([
            'message' => 'insert user success'
        ], 201);
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->back();
    }

    public function index()
    {
        $listRandomBanner = $this->banner->all()->take(4)->shuffle();
        $featureProduct = $this->product->where('active', 1)->orderBy('created_at','desc')->first();
        $randomSaleProduct = $this->product->where('active', 1)->where('sale', '!=', 0)->with('images')->get()->random();
        $listNewestProducts = $this->product->where('active', 1)->orderBy('created_at', 'desc')->get()->take(9);
        $listSellerProduct = $this->product->with('images')
            ->where('active', 1)->orderBy('pay','desc')
            ->get()->take(9);
        $listSellerProductAfterChunk = $listSellerProduct->chunk(4);
        $listHotProduct = $this->product->where('active', 1)->orderBy('view', 'desc')->get()->take(9);
        $listSpecialProduct = $this->product->where('active', 1)->get()->take(9)->shuffle();

        $categoryPhone = $this->category->findOrFail(1);
        $categoryLaptop = $this->category->findOrFail(2);
        $categoryAccessory = $this->category->findOrFail(3);
        $categoryWatch = $this->category->findOrFail(4);

        $listMixProduct = [];
        $listPhoneCategoryId = $this->category->where('parent_id', 1)->get()->pluck('id')->toArray();
        $listLaptopCategoryId = $this->category->where('parent_id', 2)->get()->pluck('id')->toArray();
        $listAccessoryCategoryId = $this->category->where('parent_id', 3)->get()->pluck('id')->toArray();
        $listWatchCategoryId = $this->category->where('parent_id', 4)->get()->pluck('id')->toArray();
        $hotPhone = $this->product->whereIn('category_id', $listPhoneCategoryId)->orderBy('pay', 'desc')->first();
        $hotLaptop = $this->product->whereIn('category_id', $listLaptopCategoryId)->orderBy('pay', 'desc')->first();
        $hotAccessory = $this->product->whereIn('category_id', $listAccessoryCategoryId)->orderBy('pay', 'desc')->first();
        $hotWatch = $this->product->whereIn('category_id', $listWatchCategoryId)->orderBy('pay', 'desc')->first();

        array_push($listMixProduct, $hotPhone);
        array_push($listMixProduct, $hotLaptop);
        array_push($listMixProduct, $hotAccessory);
        array_push($listMixProduct, $hotWatch);

        $listWishList = $this->wishList->where('user_id', Auth::id())->get()->pluck('product_id');

        return view('customer.index',
            compact(
                'listRandomBanner',
                'featureProduct',
                'randomSaleProduct',
                'listSellerProduct',
                'listNewestProducts',
                'listSellerProductAfterChunk',
                'listHotProduct',
                'listSpecialProduct',
                'categoryPhone',
                'categoryLaptop',
                'categoryAccessory',
                'categoryWatch',
                'listMixProduct',
                'listWishList'
            )
        );
    }

    public function showCategory($slug, $id)
    {
        $categoryPhone = $this->category->findOrFail(1);
        $categoryLaptop = $this->category->findOrFail(2);
        $categoryAccessory = $this->category->findOrFail(3);
        $categoryWatch = $this->category->findOrFail(4);
        $featureProduct = $this->product->where('active', 1)->orderBy('created_at','desc')->first();
        $randomSaleProduct = $this->product->where('active', 1)->where('sale', '!=', 0)->with('images')->get()->random();
        $listSellerProduct = $this->product->with('images')
            ->where('active', 1)->orderBy('pay','desc')
            ->get()->take(9);

        $categorySelected = $this->category->with('categoryChildren', 'banners')->findOrFail($id);

        $listProduct = [];

        foreach ($categorySelected->categoryChildren as $category) {
            foreach ($category->products as $product) {
                array_push($listProduct, $product);
            }
        }

        shuffle($listProduct);

        $listWishList = $this->wishList->where('user_id', Auth::id())->get()->pluck('product_id');

        return view('customer.list-product',
            compact(
            'categoryLaptop',
            'categoryPhone',
            'categoryAccessory',
            'categoryWatch',
            'featureProduct',
            'randomSaleProduct',
            'listSellerProduct',
            'categorySelected',
            'listProduct',
            'id',
            'listWishList'
        ));
    }

    public function showProduct($id, $parentId)
    {
        if ($id == 0) {
            $categorySelected = $this->category->with('categoryChildren', 'banners')->findOrFail($parentId);
            $listProduct = [];
            foreach ($categorySelected->categoryChildren as $category) {
                foreach ($category->products as $product) {
                    array_push($listProduct, $product);
                }
            }

            shuffle($listProduct);
        } else {
            $listProduct = $this->product->where('category_id', $id)->get();
        }

        $listWishList = $this->wishList->where('user_id', Auth::id())->get()->pluck('product_id');

        return view('components-customer.list-product.view-list-product', compact('listProduct', 'listWishList'));
    }

    public function showProductByPrice($parentId, Request $request)
    {
        $categorySelected = $this->category->with('categoryChildren', 'banners')->findOrFail($parentId);
        $listProduct = [];
        foreach ($categorySelected->categoryChildren as $category) {
            foreach ($category->products as $product) {
                if($product->price >= $request->minPrice && $product->price <= $request->maxPrice) {
                    array_push($listProduct, $product);
                }
            }
        }

        shuffle($listProduct);
        $listWishList = $this->wishList->where('user_id', Auth::id())->get()->pluck('product_id');

        return view('components-customer.list-product.view-list-product', compact('listProduct', 'listWishList'));
    }

    public function showDetailProduct($id, $parentId)
    {
        $categorySelected = $this->category->with('categoryChildren', 'banners')->findOrFail($parentId);
        $categoryPhone = $this->category->findOrFail(1);
        $categoryLaptop = $this->category->findOrFail(2);
        $categoryAccessory = $this->category->findOrFail(3);
        $categoryWatch = $this->category->findOrFail(4);
        $featureProduct = $this->product->where('active', 1)->orderBy('created_at','desc')->first();
        $randomSaleProduct = $this->product->where('active', 1)->where('sale', '!=', 0)->with('images')->get()->random();
        $listSellerProduct = $this->product->with('images')
            ->where('active', 1)->orderBy('pay','desc')
            ->get()->take(9);

        $productSelected = $this->product->with('images')->findOrFail($id);
        $listLatestProducts = $this->product->where('active', 1)->where('category_id', $productSelected->category_id)
            ->orderBy('created_at', 'desc')->get()->take(3);
        $listUpsellProducts = $this->product->where('category_id', $productSelected->category_id)->where('id', '!=', $id)
            ->orderBy('pay', 'desc')->get()->take(4);

        $listProductComment = $this->rating->with('ratingChildren')->where('product_id', $id)
            ->orderBy('created_at', 'desc')->get();

        Carbon::setLocale('vi');
        $now = Carbon::now();

        foreach ($listProductComment as $comment) {
            $comment['time'] = $comment->created_at->diffForHumans($now);
        }

        // increment view
        $product_key = 'product'.$id;
        if(!Session::has($product_key)){
            $this->product->where('id',$id)->increment('view');
            Session::put($product_key,1);
        }

        $listWishList = $this->wishList->where('user_id', Auth::id())->get()->pluck('product_id');


        return view('customer.detail-product',
            compact(
                'categorySelected',
                'categoryPhone',
                'categoryLaptop',
                'categoryAccessory',
                'categoryWatch',
                'featureProduct',
                'randomSaleProduct',
                'listSellerProduct',
                'productSelected',
                'listLatestProducts',
                'listUpsellProducts',
                'listProductComment',
                'listWishList'
            )
        );
    }

    public function sendComment($id, $parentId, Request $request)
    {
        try {
            DB::beginTransaction();

            $dataInsert = $request->except('_token');
            $dataInsert['product_id'] = $id;
            $dataInsert['user_id'] = Auth::id();
            $dataInsert['parent_id'] = $parentId;
            $productRating = $this->rating->create($dataInsert);

            $product = $this->product->findOrFail($id);
            $product->times_rating = $product->times_rating + 1;
            $product->total_rating = $product->time_rating + $request->rating_number;
            $product->save();

            DB::commit();

            return response()->json([
                'message' => 'success',
                'data' => $productRating
            ], 200);
        }
        catch (\Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
        }

    }

    public function sendCommentNonRating($id, $parentId, Request $request)
    {
        $dataInsert = $request->all();
        $dataInsert['product_id'] = $id;
        $dataInsert['user_id'] = Auth::id();
        $dataInsert['parent_id'] = $parentId;
        $productRating = $this->rating->create($dataInsert);

        return response()->json([
            'message' => 'success',
            'data' => $productRating
        ], 200);
    }

    public function search(Request $request)
    {
        if ($request->text_search != '') {
            $listProduct = $this->product->where('active', 1)->where('name', 'like', '%' . $request->text_search . '%')->get()->take(10);

            foreach ($listProduct as $product) {
                $product['price'] = $product->price - $product->price * $product->sale / 100;
                $product['avatar'] = asset($product->avatar);
                $product['href'] = route('frontend.show-detail-product', ['id' => $product->id, 'parentId' => $product->category_id]);
            }

            return response()->json([
                'data' => $listProduct
            ], 200);
        }
    }

    public function addToWishList($id)
    {
        $wishList = $this->wishList->firstOrCreate([
            'product_id' => $id,
            'user_id' => Auth::id()
        ]);

        return redirect()->back();
    }

    public function listWishList()
    {
        $listProduct = [];
        $listWishList = $this->wishList->where('user_id', Auth::id())->get();

        foreach ($listWishList as $wishList) {
            array_push($listProduct, $wishList->product);
        }

        return view('components-customer.modal-wishlist', compact('listProduct'));
    }

    public function removeFromWishList($id)
    {
        $this->wishList->where('user_id', Auth::id())->where('product_id', $id)->delete();

        return response()->json([
            'message' => 'remove from wishlist success'
        ], 204);
    }

    public function changeInformation(Request $request)
    {
        $dataUpdate = $request->except('_token', 'avatar');

        if($request->has('avatar')){
            $avatar = $this->storageTraitUpload($request->avatar, 'user');
            $dataUpdate['avatar'] = $avatar['file_path'];
        }

        $user = $this->user->findOrFail(Auth::id());
        $user->update($dataUpdate);

        Session::put('status-information', 1);

        return response()->json([
            'message' => 'change information success'
        ], 201);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $this->user->findOrFail(Auth::id());
        $user->password = Hash::make($request->password);
        $user->save();

        Session::put('status-information', 2);

        return response()->json([
            'message' => 'change password success'
        ], 201);
    }

    public function showDetailOrder($id)
    {
        $listDetailOrder = $this->orderDetail->where('order_id', $id)->get();

        return view('components-customer.modal-detail-order', compact('listDetailOrder'));
    }

    public function cancelOrder($id)
    {
        $order = $this->order->findOrFail($id);
        $order->status = 4;
        $order->save();

        return response()->json([
            'message' => 'cancel order success'
        ], 200);
    }

}
