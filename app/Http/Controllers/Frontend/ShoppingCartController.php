<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\WishList;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class ShoppingCartController extends Controller
{
    private $category;
    private $banner;
    private $product;
    private $article;
    private $order;
    private $orderDetail;
    private $wishList;

    public function __construct
    (
        Category $category,
        Banner $banner,
        Product $product,
        Article $article,
        Order $order,
        OrderDetail $orderDetail,
        WishList $wishList
    )
    {
        $this->category = $category;
        $this->banner = $banner;
        $this->product = $product;
        $this->article = $article;
        $this->order = $order;
        $this->orderDetail = $orderDetail;
        $this->wishList = $wishList;
    }
    public function showCartDetail()
    {
        $categoryPhone = $this->category->findOrFail(1);
        $categoryLaptop = $this->category->findOrFail(2);
        $categoryAccessory = $this->category->findOrFail(3);
        $categoryWatch = $this->category->findOrFail(4);
        $featureProduct = $this->product->where('active', 1)->orderBy('created_at','desc')->first();
        $randomSaleProduct = $this->product->where('active', 1)->where('sale', '!=', 0)->with('images')->get()->random();
        $listSellerProduct = $this->product->with('images')
            ->where('active', 1)->orderBy('pay','desc')
            ->get()->take(9);

        $status = 1;

        $listWishList = $this->wishList->where('user_id', Auth::id())->get()->pluck('product_id');

        return view('customer.cart-detail',
            compact('categoryPhone',
                'categoryLaptop',
                'categoryAccessory',
                'categoryWatch',
                'featureProduct',
                'randomSaleProduct',
                'listSellerProduct',
                'status',
                'listWishList'
            )
        );
    }

    public function addToCart(Request $request)
    {
        $product = $this->product->findOrFail($request->id);
        Cart::add([
            'id' => $product->id,
            'name' => $product->name,
            'qty' => $request->quantity,
            'price' => $product->price - $product->price * $product->sale / 100,
            'weight' => 0,
            'options' => ['image' => $product->avatar, 'sale' => $product->sale, 'price_origin' => $product->price]
        ]);

        return response()->json([
            'message' => 'Add to cart success'
        ]);
    }

    public function addToCartSimple($productId) {
        $product = $this->product->findOrFail($productId);
        Cart::add([
            'id' => $product->id,
            'name' => $product->name,
            'qty' => 1,
            'price' => $product->price - $product->price * $product->sale / 100,
            'weight' => 0,
            'options' => ['image' => $product->avatar, 'sale' => $product->sale, 'price_origin' => $product->price]
        ]);

        return redirect()->back();
    }

    public function removeProductFromCart($rowId)
    {
        Cart::remove($rowId);

        return redirect()->back();
    }

    public function updateQuantityProductFromCart($rowId, Request $request)
    {
        Cart::update($rowId, $request->quantity);

        return response()->json([
            'message' => 'Update quantity success'
        ]);
    }

    public function checkout(OrderRequest $request)
    {
        try {
            DB::beginTransaction();

            $dataInsert = $request->except('_token');

            if (Auth::check())
            {
                $dataInsert['user_id'] = Auth::id();
            }

            $dataInsert['total_money'] = (int) str_replace(',','',Cart::subtotal());

            $order = $this->order->create($dataInsert);

            foreach (Cart::content() as $item) {
                $this->orderDetail->create([
                    'order_id' => $order->id,
                    'product_id' => $item->id,
                    'price' => $item->options->price_origin,
                    'quantity' => $item->qty,
                    'sale' => $item->options->sale,
                ]);

                $product = $this->product->findOrFail($item->id);
                $product->quantity = $product->quantity - $item->qty;
                $product->save();
            }
            DB::commit();

            Cart::destroy();
            Session::put('alert-message', 1);

            return redirect()->back();

        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
        }

    }

}
