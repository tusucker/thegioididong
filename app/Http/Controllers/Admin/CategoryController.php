<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Traits\DataTree;
use App\Traits\StorageImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    use DataTree;
    use StorageImage;

    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        Session::put('isActive', 2);
        $listCategoryParent = $this->showDataTree($this->category->with('categoryParent')->get());

        return view('admin.categories.index', compact('listCategoryParent'));
    }

    public function listCategory(Request $request)
    {
        $data_input_search = $request->search;

        $categories = $this->category
            ->when($data_input_search, function ($query) use ($data_input_search) {
                return $query->where('name', 'like', '%' . $data_input_search . '%');
            })
            ->with('categoryParent')
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('admin.categories.list-category', compact('categories', 'data_input_search'));
    }

    public function listParent()
    {
        $listCategoryParent = $this->showDataTree($this->category->with('categoryParent')->get());

        return view('admin.categories.list-parent', compact('listCategoryParent'));
    }

    public function store(CategoryRequest $request)
    {

        $dataInsert = $request->except('_token','banner');
        $dataInsert['slug'] = Str::slug($request->name);
        $category = $this->category->create($dataInsert);

        if($request->has('banner')){
            foreach ($request->banner as $item) {
                $listBanner = $this->storageTraitUpload($item, 'banner');
                $category->banners()->create([
                    'path' => $listBanner['file_path'],
                    'name' => $listBanner['file_name'],
                ]);
            }
        }

        return response()->json([
            'message' => 'Insert Success',
            'data' => $category,
            'code' => 201
        ], 201);
    }

    public function update($id, CategoryRequest $request)
    {
        $category = $this->category->findOrFail($id);
        $dataUpdate = $request->except('_token');
        $dataUpdate['slug'] = Str::slug($request->name);
        $category->update($dataUpdate);

        if($request->has('banner_update')){
            $category->banners()->delete();

            foreach ($request->banner_update as $item) {
                $listBanner = $this->storageTraitUpload($item, 'banner');
                $category->banners()->create([
                    'path' => $listBanner['file_path'],
                    'name' => $listBanner['file_name'],
                ]);
            }
        }

        return response()->json([
            'message' => 'Update Success',
            'data' => $category,
            'code' => 200
        ], 200);
    }

    public function getCategory($id)
    {
        $category = $this->category->with('banners')->findOrFail($id);

        return response()->json([
            'category' => $category,
            'code' => 200
        ], 200);
    }

    public function destroy($id)
    {
        $this->category->findOrFail($id)->categoryChildren()->delete();
        $this->category->destroy($id);

        return response()->json([
            'message' => 'Delete success',
            'code' => 204
        ], 204);
    }

    public function changeStatus($id)
    {
        $category = $this->category->findOrFail($id);
        if ($category->active == 1) {
            $category->active = 0;
        } else {
            $category->active = 1;
        }
        $category->save();

        return response()->json([
            'message' => 'Change status success',
            'code' => 200
        ], 200);
    }

    public function treeView(){

        $listCategoryParent = $this->showDataTree($this->category->with('categoryParent')->get());
//        dd($listCategoryParent);

        return view('admin.categories.tree-view', compact('listCategoryParent'));
    }
}
