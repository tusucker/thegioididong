<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{
    private $role;
    private $permission;

    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }

    public function index()
    {
        Session::put('isActive', 6);
        $roles = $this->role->all();
        return view('admin.roles.index', compact('roles'));
    }

    public function create()
    {
        $listPermissionParent = $this->permission->where('parent_id', 0)->with('permissionChildren')->get();

        return view('admin.roles.create', compact('listPermissionParent'));
    }

    public function store(RoleRequest $request)
    {
        $dataInsert = $request->only(['name', 'display_name']);

        $role = $this->role->create($dataInsert);
        $role->permissions()->attach($request->permission_id);

        return redirect()->route('roles.index')->withSuccess('Create role success');
    }

    public function edit($id)
    {
        $role = $this->role->with('permissions')->findOrFail($id);
        $lisPermissionChecked = $role->permissions()->get();
        $listPermissionParent = $this->permission->where('parent_id', 0)->with('permissionChildren')->get();

        return view('admin.roles.edit', compact('role', 'lisPermissionChecked', 'listPermissionParent'));
    }

    public function update(RoleRequest $request, $id)
    {
        $dataUpdate = $request->only(['name', 'display_name']);
        $role = $this->role->findOrFail($id);
        $role->update($dataUpdate);
        $role->permissions()->sync($request->permission_id);

        return redirect()->route('roles.index')->withSuccess('Update role success');
    }

    public function destroy($id)
    {
        $this->role->destroy($id);
        return response()->json([
            'message' => 'Delete success',
            'code' => 204
        ], 204);
    }
}
