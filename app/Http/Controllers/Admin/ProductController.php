<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Tag;
use App\Traits\DataTree;
use App\Traits\StorageImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    use DataTree;
    use StorageImage;

    private  $product;
    private  $category;
    private  $tag;

    public function __construct(Product $product, Category $category, Tag $tag)
    {
        $this->product = $product;
        $this->category = $category;
        $this->tag = $tag;
    }

    public function index(){

        Session::put('isActive', 3);

        $listCategory = $this->showDataTree($this->category->with('categoryParent')->get());

        return view('admin.products.index',compact('listCategory'));
    }

    public function listProduct(Request $request){
        $data_input_search = $request->search;

        $products = $this->product
            ->when($data_input_search, function ($query) use ($data_input_search) {
                return $query->where('name', 'like', '%' . $data_input_search . '%');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('admin.products.list-product', compact('products'));
    }

    public function store(ProductRequest $request)
    {
        try{
            DB::beginTransaction();

            $dataInsert = $request->except('_token', 'avatar','tags','image_detail');
            $dataInsert['slug'] = Str::slug($request->name);

            if($request->has('avatar')){
                $avatar = $this->storageTraitUpload($request->avatar, 'avatar');
                $dataInsert['avatar'] = $avatar['file_path'];
            }

            $dataInsert['admin_id'] = auth()->id();

            $product = $this->product->create($dataInsert);

            if($request->has('image_detail')){
                foreach ($request->image_detail as $item) {
                    $imageDetail = $this->storageTraitUpload($item, 'image_detail');
                    $product->images()->create([
                        'path' => $imageDetail['file_path'],
                        'name' => $imageDetail['file_name'],
                    ]);
                }
            }
            if ($request->has('tags')) {
                foreach ($request->tags as $item) {
                    $tagInstance = $this->tag->firstOrCreate(['name' => $item]);
                    $tagIds[] = $tagInstance->id;
                }
                $product->tags()->attach($tagIds);
            }

            DB::commit();
        }
        catch (\Exception $exception){
            Log::error($exception->getMessage());
            DB::rollBack();
        }


        return response()->json([
            'message' => 'Insert Success',
            'data' => $product,
            'code' => 201
        ], 201);
    }

    public function edit($id)
    {
        $listCategory = $this->showDataTree($this->category->with('categoryParent')->get());
        $product = $this->product->with('tags', 'images', 'categories')->findOrFail($id);

        // get list option category
        $viewCategory = '';
        foreach ($listCategory as $category){
            if ($category->id == $product->category_id) {
                $viewCategory .= "<option selected value='" . $category->id . "'>" . str_repeat('--',$category->level).$category->name. "</option>";
            }
            else {
                $viewCategory .= "<option value='" . $category->id . "'>" . str_repeat('--',$category->level).$category->name. "</option>";
            }
        }

        // get option tags
        $viewTag = '';
        foreach ($product->tags as $tag){
            $viewTag .= "<option selected value='" . $tag->id . "'>" . $tag->name . "</option>";
        }

        return response()->json([
            'listCategory' => $listCategory,
            'product' => $product,
            'viewCategory' => $viewCategory,
            'viewTag' => $viewTag
        ], 200);
    }

    public function update($id, ProductRequest $request)
    {
        try {
            DB::beginTransaction();

            $product = $this->product->findOrFail($id);
            $dataUpdate = $request->except('_token', 'avatar','tags');
            $dataUpdate['slug'] = Str::slug($request->name);

            if($request->has('avatar')){
                $avatar = $this->storageTraitUpload($request->avatar, 'avatar');
                $dataUpdate['avatar'] = $avatar['file_path'];
            }

            $product->update($dataUpdate);

            if ($request->has('tags')) {
                foreach ($request->tags as $tag) {
                    $tagInstance = $this->tag->firstOrCreate(['name' => $tag]);
                    $tagIds[] = $tagInstance->id;
                }
                $product->tags()->sync($tagIds);
            }

            DB::commit();
        }
        catch (\Exception $exception){
            Log::error($exception->getMessage());
            DB::rollBack();
        }
    }

    public function destroy($id)
    {
        $this->product->destroy($id);

        return response()->json([
            'message' => 'delete success',
            'code' => 204
        ],204);
    }

    public function changeStatus($id)
    {
        $product = $this->product->findOrFail($id);
        if ($product->active == 1) {
            $product->active = 0;
        } else {
            $product->active = 1;
        }
        $product->save();

        return response()->json([
            'message' => 'Change status success',
            'code' => 200
        ], 200);
    }

}
