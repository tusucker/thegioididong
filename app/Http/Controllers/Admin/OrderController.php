<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    private $order;
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function index()
    {
        Session::put('isActive', 7);
        $listOrder = $this->order->paginate(20);

        return view('admin.orders.index', compact('listOrder'));
    }

    public function detail($id)
    {
        $order = $this->order->with('orderDetail')->findOrFail($id);

        return view('admin.orders.modal-detail', compact('order'));
    }

    public function changeStatusToTransport($id)
    {
        $order = $this->order->findOrFail($id);
        $order->status = 2;
        $order->save();

        return redirect()->back()->withSuccess('Change Status To Transporting Success');
    }

    public function changeStatusToDone($id)
    {
        $order = $this->order->findOrFail($id);
        $order->status = 3;
        $order->save();

        return redirect()->back()->withSuccess('Change Status To Done Success');
    }
}
