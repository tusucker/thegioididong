<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Traits\StorageImage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    use StorageImage;
    protected $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function index()
    {
        Session::put('isActive', 4);

        $articles = $this->article->all();
        return view('admin.articles.index', compact('articles'));
    }

    public function create()
    {
        return view('admin.articles.create');
    }

    public function store(ArticleRequest $request)
    {
       $dataInsert = $request->except('_token', 'avatar');
       $dataInsert['slug'] = Str::slug($request->name);
       $dataInsert['author'] = auth()->id();

       if($request->has('avatar')) {
           $avatar = $this->storageTraitUpload($request->avatar, 'avatar');
           $dataInsert['avatar'] = $avatar['file_path'];
       }

       $article = $this->article->create($dataInsert);

       if ($article) {
           return redirect()->route('articles.index')->withSuccess('Create article success');
       }
       else {
           return redirect()->back()->withErrors();
       }
    }

    public function edit($id)
    {
        $article = $this->article->findOrFail($id);

        return view('admin.articles.edit', compact('article'));
    }

    public function update($id, ArticleRequest $request)
    {
        $dataUpdate = $request->except('_token', 'avatar');
        $dataUpdate['slug'] = Str::slug($request->name);
        $dataUpdate['author'] = auth()->id();

        if($request->has('avatar')) {
            $avatar = $this->storageTraitUpload($request->avatar, 'avatar');
            $dataUpdate['avatar'] = $avatar['file_path'];
        }

        $article = $this->article->findOrFail($id);
        $article->update($dataUpdate);

        return redirect()->route('articles.index')->withSuccess('Update article success');
    }

    public function destroy($id)
    {
        $this->article->destroy($id);
    }
}
