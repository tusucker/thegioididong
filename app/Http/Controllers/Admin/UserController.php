<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\Role;
use App\Traits\StorageImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    use StorageImage;

    private $user;
    private $role;
    public function __construct (User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    public function index()
    {
        Session::put('isActive', 5);
        $roles = $this->role->all();
        $users = $this->user->with('roles')->get();

        return view('admin.users.index', compact('users', 'roles'));
    }

    public function create()
    {
        $roles = $this->role->all();
        return view('admin.users.create', compact('roles'));
    }

    public function store(UserRequest $request)
    {
        try {
            DB::beginTransaction();

            $dataInsert = $request->except('_token', 'avatar', 'role', 'password');
            if($request->has('avatar')){
                $avatar = $this->storageTraitUpload($request->avatar, 'user');
                $dataInsert['avatar'] = $avatar['file_path'];
            }

            $dataInsert['password'] = Hash::make($request->password);

            $user = $this->user->create($dataInsert);
            $roles = $request->role;
            $user->roles()->attach($roles);

            DB::commit();

            return redirect()->route('users.index')->withSuccess('Create user success');
        }
        catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    public function edit($id)
    {
        $user = $this->user->with('roles')->findOrFail($id);
        $roles = $this->role->all();
        $listRoleUserId = $user->roles()->get()->pluck('id')->toArray();

        return view('admin.users.edit', compact('user', 'roles', 'listRoleUserId'));
    }

    public function update($id, UserRequest $request)
    {
        try {
            DB::beginTransaction();

            $dataUpdate = $request->except('_token', 'avatar', 'role', 'password');
            if($request->has('avatar')){
                $avatar = $this->storageTraitUpload($request->avatar, 'user');
                $dataUpdate['avatar'] = $avatar['file_path'];
            }

            if($request->has('password') ) {
                $dataUpdate['password'] = Hash::make($request->password);
            }

            $user = $this->user->findOrFail($id);
            $user->update($dataUpdate);
            $roles = $request->role;
            $user->roles()->sync($roles);

            DB::commit();

            return redirect()->route('users.index')->withSuccess('Update user success');
        }
        catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    public function destroy($id)
    {
        $this->user->destroy($id);
        return response()->json([
            'message' => 'Delete success',
            'code' => 204
        ], 204);
    }
}
