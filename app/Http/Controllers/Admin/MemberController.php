<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function showDashboard(){
        Session::put('isActive',1);
        return view('admin.dashboard.index');
    }
}
