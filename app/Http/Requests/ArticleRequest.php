<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:articles,name,' . $this->id,
            'content' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Article title is not null',
            'name.unique' => 'This title is in used',
            'content.required' => 'Article content is not null'
        ];
    }
}
