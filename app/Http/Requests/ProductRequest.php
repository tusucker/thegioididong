<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required| unique:products,name,' . $this->id,
            'price' => 'required| numeric| min:0',
            'sale' => 'integer| min:0| max: 100',
            'category_id' => 'required',
            'quantity' => 'required|integer|min:0'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Product name must be not null',
            'name.unique' => 'This name is existed',
            'price.numeric' => 'Product price must be numeric',
            'price.required' => 'Product price must be not null',
            'price.min' => 'Product price must be above than 0',
            'sale.integer' => 'Product sale must be integer',
            'sale.min' => 'Product sale must be above than 0',
            'sale.max' => 'Product sale must be under than 100',
            'quantity.required' => 'Quantity must be not null',
            'quantity.integer' => 'Quantity must be integer',
            'quantity.min' => 'Quantity must be above than 0',
        ];
    }
}
