<?php

namespace App\Http\Requests;

use App\Rules\CheckOldPassword;
use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'old_password' => ['required', new CheckOldPassword()],
            'password' => 'required',
            're_password' => 'required|same:password'
        ];
    }

    public function messages()
    {
        return [
            'old_password.required' => 'Mật khẩu cũ không được để trống',
            'password.required' => 'Mật khẩu không được để trống',
            're_password.required' => 'Mật khẩu lần 2 không được để trống',
            're_password.same' => 'Mật khẩu phải trùng khớp',
        ];
    }
}
