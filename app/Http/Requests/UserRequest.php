<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users,email,'. $this->id,
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Field name is required',
            'email.required' => 'Field email is required',
            'email.unique' => 'This email is in used',
        ];
    }
}
