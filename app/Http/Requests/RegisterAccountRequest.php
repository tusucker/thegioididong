<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            're_password' => 'required|same:password'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên của bạn không được để trống',
            'email.required' => 'Email của bạn không được để trống',
            'email.unique' => 'Email này đã sử dụng',
            'password.required' => 'Mật khẩu không được để trống',
            're_password.required' => 'Mật khẩu lần 2 không được để trống',
            're_password.same' => 'Mật khẩu phải trùng khớp',
        ];
    }
}
