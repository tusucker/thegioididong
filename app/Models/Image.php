<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $guarded = [];

    // relationship
    public function products(){
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
