<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    // relationship
    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }
}
