<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table='categories';
    protected $fillable=[
        'parent_id',
        'name',
        'slug',
        'active'
    ];

    // relationships

    public function categoryParent(){
        return $this->belongsTo(Category::class,'parent_id','id');
    }

    public function categoryChildren(){
        return $this->hasMany(Category::class,'parent_id','id');
    }

    public function banners(){
        return $this->hasMany(Banner::class,'category_id','id');
    }

    public function products(){
        return $this->hasMany(Product::class,'category_id','id');
    }
}
