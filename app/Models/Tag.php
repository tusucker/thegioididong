<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method firstOrCreate(array $array)
 */
class Tag extends Model
{
    protected $guarded = [];

    // relationship

    public function products(){
        return $this->belongsToMany(Product::class,'product_tags','tag_id','product_id');
    }
}
