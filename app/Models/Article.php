<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'author', 'id');
    }

    public function articleComments()
    {
        return $this->hasMany(ArticleComment::class, 'article_id', 'id');
    }
}
