<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method create(array $dataInsert)
 * @method orderBy(string $string, string $string1)
 * @method findOrFail($id)
 */
class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    protected $guarded = [];

    // relationship

    public function categories(){
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function users(){
        return $this->belongsTo(User::class,'admin_id','id');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'product_tags','product_id','tag_id');
    }

    public function images(){
        return $this->hasMany(Image::class,'product_id','id');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'product_id', 'id');
    }

    // accessors

    public function getAverageStarAttribute()
    {
        return $this->times_rating != 0 ? ( (float) $this->total_rating / $this->times_rating ) : 0;
    }

}
