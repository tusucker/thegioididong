<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moduleName = ['dashboard', 'category', 'product', 'article', 'user', 'role'];

        \App\Models\Permission::truncate();

        for ($i=0; $i<6; $i++) {
            \App\Models\Permission::create([
                'id' => $i + 1,
                'name' => $moduleName[$i],
                'display_name' => $moduleName[$i],
                'parent_id' => 0,
            ]);
        }

        for ($i=0; $i<6; $i++) {
            if ($i == 0) {
                \App\Models\Permission::create([
                    'name' => 'view ' . $moduleName[$i],
                    'display_name' => 'view ' . $moduleName[$i],
                    'parent_id' => $i + 1,
                    'key_code' => 'view_' . $moduleName[$i]
                ]);
            }
            else {
                \App\Models\Permission::create([
                    'name' => 'view ' . $moduleName[$i],
                    'display_name' => 'view ' . $moduleName[$i],
                    'parent_id' => $i + 1,
                    'key_code' => 'view_' . $moduleName[$i]
                ]);
                \App\Models\Permission::create([
                    'name' => 'add ' . $moduleName[$i],
                    'display_name' => 'add ' . $moduleName[$i],
                    'parent_id' => $i + 1,
                    'key_code' => 'add_' . $moduleName[$i]
                ]);
                \App\Models\Permission::create([
                    'name' => 'edit ' . $moduleName[$i],
                    'display_name' => 'edit ' . $moduleName[$i],
                    'parent_id' => $i + 1,
                    'key_code' => 'edit_' . $moduleName[$i]
                ]);
                \App\Models\Permission::create([
                    'name' => 'delete ' . $moduleName[$i],
                    'display_name' => 'delete ' . $moduleName[$i],
                    'parent_id' => $i + 1,
                    'key_code' => 'delete_' . $moduleName[$i]
                ]);
            }

        }

    }
}
