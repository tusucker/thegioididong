<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'=>'Lê Văn Thục',
            'email'=>'lethuc1308@gmail.com',
            'password'=>Hash::make('lethuc98')
        ]);
    }
}
