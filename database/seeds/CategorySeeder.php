<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Category::create([
                'name' => 'Điện thoại',
                'slug' => \Illuminate\Support\Str::slug('Điện thoại', '-')]
        );
        \App\Models\Category::create([
                'name' => 'Laptop',
                'slug' => \Illuminate\Support\Str::slug('Laptop', '-')]
        );
        \App\Models\Category::create([
                'name' => 'Phụ kiện',
                'slug' => \Illuminate\Support\Str::slug('Phụ kiện', '-')]
        );
        \App\Models\Category::create([
                'name' => 'Đồng hồ',
                'slug' => \Illuminate\Support\Str::slug('Đồng hồ', '-')]
        );
    }
}
