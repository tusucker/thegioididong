<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('name')->nullable();
            $table->string('slug')->unique();
            $table->integer('price')->default(0);
            $table->integer('category_id')->unsigned()->default(0);
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->integer('admin_id')->unsigned()->default(0);
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('sale')->default(0);
            $table->string('avatar')->nullable();
            $table->integer('view')->default(0);
            $table->integer('active')->default(1)->comment('1:active,0:inactive');
            $table->integer('pay')->default(0);
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
