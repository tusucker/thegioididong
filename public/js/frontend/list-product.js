$(function () {
  $(document).on('click', '.category-children', function () {
    $(this).parent().find('.active-category').removeClass('active-category');
    $(this).addClass('active-category');
    
    let url = $(this).data('action');
    
    callApi('get', url, null).then((rs) => {
      $('.products-grid').replaceWith(rs);
    });
  });
  
  $(document).on('click', '.fillter', function () {
    let minPrice = $('#minPrice').val();
    let maxPrice = $('#maxPrice').val();
    let url = $(this).data('action');
    
    let data = {
      minPrice: minPrice,
      maxPrice: maxPrice
    };
    callApi('get', url, data).then((rs) => {
      $('.products-grid').replaceWith(rs);
    });
    
  });
});