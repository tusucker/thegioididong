$(function () {
  $(document).on('click', '.update-quantity', function (e) {
    e.preventDefault();
    let quantity = $(this).prev().val();
    let url = $(this).attr('href');
    let data = {
      quantity: quantity
    }
  
    callApi('get', url, data).then(() => {
      location.reload();
    });
  });
  
  setTimeout(function () {
    $('.text-danger').hide();
  }, 5000);
  
  if (parseInt($('#alert-message').val()) === 1) {
    Swal.fire('Bạn đã đặt hàng thành công!')
  }
  
});