$(function () {
  let product;
  
  $(document).on('click', '.btn-cart', function () {
    product = $(this).data('product');
    $('.product-name').text(product.name);
    $('.price-product-real').text(product.price - product.price * product.sale / 100);
    $('.price-product-origin').text(product.price);
    if(parseInt(product.price) === 0) {
      $('.price-product-origin').css('display', 'none');
    }
    $('.avatar-product').attr('src', product.avatar);
    $('#modal-add-to-cart').modal('show');
  });
  
  $(document).on('click', '.btn-add-to-cart', function () {
    let productQuantity = product.quantity;
    let quantity = $('.quantity-product').val()
    if (quantity <= 0) {
      $('#modal-add-to-cart').modal('hide');
      Swal.fire('Bạn chọn số lượng không hợp lệ')
    }
    else if (quantity > productQuantity)
    {
      $('#modal-add-to-cart').modal('hide');
      Swal.fire('Bạn chọn số lượng vượt quá số lượng sản phẩm có sẵn')
    }
    else
    {
      let url = $(this).data('action');
      let data = {
        quantity: quantity,
        id: product.id
      }
      callApi('get', url, data).then(() => {
        $('#modal-add-to-cart').modal('hide');
        location.reload();
      });
    }
  });
  
  $(document).on('click', '.send-comment-product', function () {
    if (parseInt($('.id-user').val()) === 0) {
      $('#loginModal').modal('show');
    }
    else {
      let formSendComment = $('#form-send-comment');
      let url = formSendComment.attr('action');
      let data = formSendComment.serialize();
      callApi('get', url, data).then(() => {
        location.reload();
      });
    }
  });
  
  $(document).on('click', '.send-comment-simple', function () {
    if (parseInt($('.id-user').val()) === 0) {
      $('#loginModal').modal('show');
    }
    else {
      let url = $(this).data('action');
      let content = $(this).prev().find('.content').val();
      let data = {
        content: content
      }
      callApi('get', url, data).then(() => {
        location.reload();
      });
    }
  });
  
  $('.answer-comment-product').on('click', function () {
    if ($(this).attr('data-click-state') == 1) {
      $(this).attr('data-click-state', 0)
      $(this).parent().next().css('display', 'none');
    } else {
      $(this).attr('data-click-state', 1)
      $(this).parent().next().css('display', 'block');
    }
  });
});