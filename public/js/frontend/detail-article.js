$(function () {
  $(document).on('click', '.send-comment', function () {
    if (parseInt($('.id-user').val()) === 0) {
      $('#loginModal').modal('show');
    }
    else {
      let url = $(this).data('action');
      let content = $(this).prev().find('.content').val();
      let data = {
        content: content
      }
      callApi('get', url, data).then(() => {
        location.reload();
      });
    }
  });
  
  $('.answer').on('click', function () {
    if ($(this).attr('data-click-state') == 1) {
      $(this).attr('data-click-state', 0)
      $(this).parent().next().css('display', 'none');
    } else {
      $(this).attr('data-click-state', 1)
      $(this).parent().next().css('display', 'block');
    }
  });
  
});