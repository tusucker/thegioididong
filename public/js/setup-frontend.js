$(function () {
  $(document).on('click', '.open-login-form', function (e) {
    e.preventDefault();
    $('#loginModal').modal('show');
  });
  
  $(document).on('click', '.btn-login', function () {
    let formLogin = $('#form-login');
    let data = formLogin.serialize();
    let url = formLogin.attr('action');
    callApi('post', url, data).then((rs) => {
      if(rs.status === 1) {
        $('#loginModal').modal('hide');
        location.reload();
      }
      else if(rs.status === 2) {
        $('.message-error').css('display', 'block');
        setTimeout(function () {
          $('.message-error').css('display', 'none');
        }, 3000);
      }
    });
  });
  
  $(document).on('click', '.open-register-form', function (e) {
    e.preventDefault();
    $('#registerModal').modal('show');
  });
  
  $(document).on('click', '.btn-register', function () {
    let formRegister = $('#form-register');
    let data = formRegister.serialize();
    let url = formRegister.attr('action');
    callApi('post', url, data)
      .done(function () {
        Swal.fire('Bạn đã tạo tài khoản thành công')
        $('#registerModal').modal('hide');
      })
      .fail(function (rs) {
        if (rs.responseJSON.errors.name) {
          $('.message-error-name').text(rs.responseJSON.errors.name);
          $('.message-error-name').css('display', 'block');
        }
        if (rs.responseJSON.errors.email) {
          $('.message-error-email').text(rs.responseJSON.errors.email);
          $('.message-error-email').css('display', 'block');
        }
        if (rs.responseJSON.errors.password) {
          $('.message-error-password').text(rs.responseJSON.errors.password);
          $('.message-error-password').css('display', 'block');
        }
        if (rs.responseJSON.errors.re_password) {
          $('.message-error-re-password').text(rs.responseJSON.errors.re_password);
          $('.message-error-re-password').css('display', 'block');
        }
        
        setTimeout(function () {
          $('.message-error').css('display', 'none');
        }, 4000);
      });
  });
  
  $('#live-search').keyup(function () {
    let showResultSearch = $('.show-result-search');
    let url = $(this).data('action');
    let value = $(this).val();
    let data = {
      text_search: value
    }
    callApi('get', url, data)
      .then((rs) => {
        showResultSearch.empty();
        if (rs.data.length === 0) {
          showResultSearch.append(`
            <div style="margin-top: -20px; padding-left: 90px;"> Không có kết quả nào </div>
          `);
        }
        else {
          rs.data.forEach(function (item) {
            showResultSearch.append(`
              <div class="row" style="border-bottom: 1px solid yellow; margin: 0 0 5px 0;">
                <div class="col-md-3">
                    <img src="${item.avatar}" alt="">
                </div>
                <div class="col-md-9" style="padding-left: 30px; ">
                    <div style="height: 40px; overflow: hidden"><a href="${item.href}">${item.name}</a></div>
                    <div class="ratings">
                        <div class="rating-box">
                            <div class="rating" style="width: 40%"></div>
                        </div>
                    </div>
                    <div>${item.price} VND</div>
                </div>
              </div>
            `);
          });
        }
        showResultSearch.css('display', 'block');
      });
  });
  
  $(document).on('click', '.my-wishlist', function (e) {
    e.preventDefault();
    let url = $(this).data('action');
    callApi('get', url, null).then(function (rs) {
      $('#modalWishList').replaceWith(rs);
      $('#modalWishList').modal('show');
    });
  });
  
  $(document).on('click', '.remove-wishlist-item', function () {
    let selector = $(this).parent();
    let url = $(this).data('action');
    callApi('get', url, null).then(function () {
      selector.remove();
    });
  });

  $(document).on('hide.bs.modal','#modalWishList', function () {
    location.reload();
  });

  $(document).on('click', '.top-link-myaccount', function (e) {
    e.preventDefault();
    $('#modalProfile').modal('show');
  });
  
  $("#avatar").change(function () {
    readURL(this);
  });
  
  $('.btn-change-information').on('click', function () {
    let formChangeInformation = $('#form-change-information');
    let url = formChangeInformation.attr('action');
    let data = new FormData(formChangeInformation[0]);
    
    callApiWithFile('post', url, data)
      .done(function () {
        location.reload();
      });
  });
  
  $('.btn-change-password').on('click', function () {
    let formChangePassword = $('#form-change-password');
    let url = formChangePassword.attr('action');
    let data = formChangePassword.serialize();
    
    callApi('post', url, data)
      .done(function () {
        location.reload();
      })
      .fail(function (rs) {
        if (rs.responseJSON.errors.old_password) {
          $('.message-error-old-password').text(rs.responseJSON.errors.old_password);
          $('.message-error-old-password').css('display', 'block');
        }
        if (rs.responseJSON.errors.password) {
          $('.message-error-password').text(rs.responseJSON.errors.password);
          $('.message-error-password').css('display', 'block');
        }
        if (rs.responseJSON.errors.re_password) {
          $('.message-error-re-password').text(rs.responseJSON.errors.re_password);
          $('.message-error-re-password').css('display', 'block');
        }
        setTimeout(function () {
          $('.message-error').css('display', 'none');
        }, 4000);
      });
  });
  
  $(document).on('click', '.btn-detail-order', function () {
    let url = $(this).data('action');
    callApi('get', url, null).then(function (rs) {
      $('#modalDetailOrder').replaceWith(rs);
      $('#modalDetailOrder').modal('show');
    });
  });
  
  $(document).on('click', '.btn-cancel-order', function () {
    let selectorCancel = $(this);
    let selectorStatus = $(this).parent().siblings('.status-order');
    let url = $(this).data('action');
    callApi('get', url, null).then(function () {
      selectorCancel.remove();
      selectorStatus.empty().append('<span class="badge badge-danger" style="background: red">Đã hủy</span>');
    });
  });
  
  $('#modalProfile').modal('show');
  
  if (parseInt($('#status-information').val()) === 1)
  {
    Swal.fire('Bạn đã thay đổi thông tin cá nhân thành công !')
  }
  
  if (parseInt($('#status-information').val()) === 2)
  {
    Swal.fire('Bạn đã thay đổi mật khẩu thành công !')
  }
  
});

// call Api without file input
function callApi(method,url,data){
  return $.ajax({
    method:method,
    url:url,
    data:data
  });
}

// call Api with file input
function callApiWithFile(method,url,data){
  return $.ajax({
    method:method,
    url:url,
    data:data,
    processData: false,
    contentType: false,
  });
}

// prepare to call ajax
$(function (){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
});

// simple image
function readURL(input) {
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = function (e) {
      $('.show-image').children().attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}