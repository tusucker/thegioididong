const GET_METHOD = 'GET';
const POST_METHOD = 'POST';

$(function (){
    // setting for select option
    $('.select2').select2();
    
    // setting for select option multiple
    $(".choose_tags").select2({
        tags: true,
        tokenSeparators: [',']
    })
    
    // setting for tinyMCE
    var editor_config = {
        path_absolute : "/",
        selector: "textarea.my-editor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
            
            var cmsURL = editor_config.path_absolute + 'filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }
            
            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };
    
    tinymce.init(editor_config);
    
    // hide message success and error
    setTimeout(function () {
        $('.alert-success').hide();
        $('.alert-danger').hide();
    }, 5000);
    
    $("#main-image").change(function () {
        readURL(this);
    });
    
});

// hidden error
function hiddenError() {
    $('.message-error').delay(6000).fadeOut();
}

// call Api without file input
function callApi(method,url,data){
    return $.ajax({
        method:method,
        url:url,
        data:data
    });
}

// call Api with file input
function callApiWithFile(method,url,data){
    return $.ajax({
        method:method,
        url:url,
        data:data,
        processData: false,
        contentType: false,
    });
}

// prepare to call ajax
$(function (){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

// response list
function responseList(selector) {
    let url = selector.data('action');
    callApi(GET_METHOD, url, null).then((rs) => {
        selector.replaceWith(rs);
    });
}

// simple image
function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $('.show-image').children().attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}




