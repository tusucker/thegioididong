
function responseTreeView() {
  let url = $('.myadmin-dd').data('action');
  callApi(GET_METHOD, url, null).then((rs) => {
    $('.myadmin-dd').replaceWith(rs);
  });
}

function responseListParent() {
  let url = $('.list-parent').data('action');
  callApi(GET_METHOD, url, null).then((rs) => {
    $('.list-parent').replaceWith(rs);
  });
}

function fetch_data(page) {
  $.ajax({
    url: "admin/categories/list-category?page=" + page,
    success: function (data) {
      $('#list-category').html(data);
    }
  });
}

function readURL(input, index) {
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    
    reader.onload = function (e) {
      $('.visible-image-' + index).children().replaceWith('<img src="' + e.target.result + '" alt="image">');
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function handleImage() {
  for (let i = 1; i <= 8; i++) {
    $("#image-" + i).change(function () {
      readURL(this, i);
    });
  }
}

$(function () {
  
  responseList($('#list-category'));
  responseTreeView();
  responseListParent();
  
  let url_update;
  let url_delete;
  
  handleImage();
  
  let message_error = $('.message-error');
  
  //open modal add category
  $(document).on('click', '.open-modal-add-category', function () {
    $('#form-add-category')[0].reset();
    $('#modal-add-category').modal('show');
  });
  
  //click btn save to insert new category
  $(document).on('click', '#btn-save-category', function () {
    let url = $(this).data('action');
    let data = new FormData($('#form-add-category')[0]);
    
    Swal.fire({
      title: 'Do you want to save the changes?',
      showCancelButton: true,
      confirmButtonText: `Save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        callApiWithFile(POST_METHOD, url, data)
          .then(() => {
            responseList($('#list-category'));
            responseTreeView();
            responseListParent();
            $('#modal-add-category').modal('hide');
            $('#form-add-category')[0].reset();
            Swal.fire('Saved!', '', 'success')
          })
          .catch((rs) => {
            message_error.children('span').html(rs.responseJSON.errors.name);
            message_error.css('display', 'block');
            setTimeout(hiddenError);
          });
      }
    })
  });
  
  //click into tag a to get link update category and open modal update
  $(document).on('click', '.update-category', function () {
    url_update = $(this).data('action');
    
    let url = $(this).data('url');
    callApi(GET_METHOD, url, null).then((rs) => {
      $('#name').val(rs.category.name);
      if (rs.category.active === 1) {
        $("#update_active").prop("checked", true);
      } else {
        $("#update_inactive").prop("checked", true);
      }
      $(".category-parent").val(rs.category.parent_id);
      
      if (rs.category.banners.length > 0) {
        for (let i = 0; i < 4; i++) {
          $('.visible-image-' + (i + 5)).children().replaceWith('<img src="' + rs.category.banners[i].path + '" alt="image">');
        }
      } else {
        for (let i = 0; i < 4; i++) {
          $('.visible-image-' + (i + 5)).children().replaceWith('<div></div>');
        }
      }
      
    });
    $('#modal-update-category').modal('show');
  });
  
  //click button update category
  $(document).on('click', '#btn-update-category', function () {
    
    let data = new FormData($('#form-update-category')[0]);
    
    Swal.fire({
      title: 'Do you want to save the changes?',
      showCancelButton: true,
      confirmButtonText: `Save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        callApiWithFile(POST_METHOD, url_update, data)
          .then(() => {
            responseList($('#list-category'));
            responseTreeView();
            responseListParent();
            $('#modal-update-category').modal('hide');
            Swal.fire('Saved!', '', 'success');
          })
          .catch((rs) => {
            message_error.children('span').html(rs.responseJSON.errors.name);
            message_error.css('display', 'block');
            setTimeout(hiddenError);
          });
      }
    })
  });
  
  //click into tag a to get link delete category
  $(document).on('click', '.delete-category', function () {
    url_delete = $(this).data('action');
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        callApi(GET_METHOD, url_delete, null).then(() => {
          responseList($('#list-category'));
          responseTreeView();
          responseListParent();
        });
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
  });
  
  //click to change status
  $(document).on('click', '.change-status', function () {
    let url = $(this).data('action');
    callApi(GET_METHOD, url, null).then(() => {
      responseList($('#list-category'));
    });
  });
  
  //pagination
  $(document).on('click', '.pagination a', function (event) {
    event.preventDefault();
    let page = $(this).attr('href').split('page=')[1];
    fetch_data(page);
  });
  
  //search by name
  $(document).on('keyup', '#search', function () {
    let url = $('#list-category').data('action');
    let data = $('#form-search').serialize();
    callApi(GET_METHOD, url, data).then((rs) => {
      $('#list-category').replaceWith(rs);
    });
  });
  
});
