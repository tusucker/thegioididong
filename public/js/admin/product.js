// simple image
function readURL(input) {
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = function (e) {
      $('.show-image').children().attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

// multiple image
function imagesPreview(input) {
  if (input.files) {
    let filesAmount = input.files.length;
    for (i = 0; i < filesAmount; i++) {
      let reader = new FileReader();
      reader.onload = function (event) {
        $('.list-image-detail .row').append('<div class="col-4 mt-15" style="height: 250px; overflow: hidden">' +
          '<img src="' + event.target.result + '" alt="image"></div>');
      }
      reader.readAsDataURL(input.files[i]);
    }
  }
}

function fetchData(url){
  // let url = $('#list-product').data('action') + '?page=';
  callApi(GET_METHOD, url, null).then((rs) => {
    $('#list-product').html(rs);
  });
}


$(function () {
  fetchData(1);
  
  let message_error = $('.message-error');
  let url_update;
  
  responseList($('#list-product'));
  
  //open modal add product
  $(document).on('click', '.open-modal-add-product', function () {
    $('.modal-content').animate({scrollTop: 0}, "slow");
    $('#form-add-product')[0].reset();
    $('.show-image').children().attr('src', 'https://ipsumimage.appspot.com/300x280');
    $('.list-image-detail .row').empty();
    $('#tags').select2("val", "All");
    $('#category_id').val(null).trigger('change');
    $('#modal-add-product').modal('show');
  });
  
  $("#main-image").change(function () {
    readURL(this);
  });
  
  $("#main-image-update").change(function () {
    readURL(this);
  });
  
  $(document).on('change', '#image-detail', function () {
    imagesPreview(this);
  });
  
  //click btn save to insert new product
  $(document).on('click', '#btn-save-product', function () {
    let url = $(this).data('action');
    // save value textarea into data
    tinyMCE.get("getData").save();
    
    let data = new FormData($('#form-add-product')[0]);
    
    Swal.fire({
      title: 'Do you want to save the changes?',
      showCancelButton: true,
      confirmButtonText: `Save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        callApiWithFile(POST_METHOD, url, data)
          .then(() => {
            responseList($('#list-product'));
            $('#modal-add-product').modal('hide');
            Swal.fire('Saved!', '', 'success')
          })
          .catch((rs) => {
            message_error.empty();
            $.each(rs.responseJSON.errors, function (key, value) {
              message_error.append('<div>' + value + '</div>');
            });
            
            message_error.css('display', 'block');
            $('.modal-content').animate({scrollTop: 0}, "slow");
            setTimeout(hiddenError);
          });
      }
    })
  });
  
  // click show modal edit product
  $(document).on('click', '.update-product', function (){
    let url_get_data = $(this).data('url');
    url_update = $(this).data('action');
    callApi(GET_METHOD, url_get_data, null).then((rs) => {
      $('#name').val(rs.product.name);
      $('#price').val(rs.product.price);
      $('#sale').val(rs.product.sale);
      $('#description').val(rs.product.description);
      $('#quantity').val(rs.product.quantity);
      $('#category_update_id').append(rs.viewCategory);
      $('#tags_update').empty().append(rs.viewTag);
      $('.show-image-update').children().attr('src', rs.product.avatar);
      tinyMCE.get('getDataUpdate').setContent(rs.product.content);
      if (rs.product.active === 1) {
        $("#active_update").prop("checked", true);
      } else {
        $("#inactive_update").prop("checked", true);
      }
      
      $('#modal-update-product').modal('show');
    });
  });
  
  // click save to update product
  $(document).on('click', '#btn-save-product-update', function (){
    tinyMCE.get("getDataUpdate").save();
    let data = new FormData($('#form-update-product')[0]);
  
    Swal.fire({
      title: 'Do you want to save the changes?',
      showCancelButton: true,
      confirmButtonText: `Save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        callApiWithFile(POST_METHOD, url_update, data)
          .then(() => {
            responseList($('#list-product'));
            $('#modal-update-product').modal('hide');
          })
          .catch((rs) => {
            message_error.empty();
            $.each(rs.responseJSON.errors, function (key, value) {
              message_error.append('<div>' + value + '</div>');
            });
  
            message_error.css('display', 'block');
            $('.modal-content').animate({scrollTop: 0}, "slow");
            setTimeout(hiddenError);
          });
      }
    })
  });
  
  // click to delete product
  $(document).on('click', '.delete-product', function (){
    let url = $(this).data('action');
  
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        callApi(GET_METHOD, url, null).then(() => {
          responseList($('#list-product'));
        });
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
  });
  
  // click to change status
  $(document).on('click', '.change-status', function (){
    let url = $(this).data('action');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34495e',
      cancelButtonColor: '#bdc3c7',
      confirmButtonText: 'Yes, change it!'
    }).then((result) => {
      if (result.isConfirmed) {
        callApi(GET_METHOD, url, null).then(() => {
          responseList($('#list-product'));
        });
        Swal.fire(
          'Change Success!',
          'Your file has been changed.',
          'success'
        )
      }
    })
  });
  
  // click to page number
  $(document).on('click', '.pagination a', function (event) {
    event.preventDefault();
    let url = $(this).attr('href');
    fetchData(url);
  });
  
  // search by name
  $(document).on('keyup', '#search', function () {
    let url = $('#list-product').data('action');
    let data = $('#form-search').serialize();
    callApi(GET_METHOD, url, data).then((rs) => {
      $('#list-product').replaceWith(rs);
    });
  });
  
});

