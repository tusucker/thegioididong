$(function () {
  
  function readURL(input) {
    if (input.files && input.files[0]) {
      let reader = new FileReader();
      reader.onload = function (e) {
        $('.show-image').children().attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }
  
  $("#main-image").change(function () {
    readURL(this);
  });
  
  $(document).on('click', '.delete-article', function (e) {
    e.preventDefault();
    let selector = $(this).parents('tr');
    let url = $(this).attr('href');
  
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        callApi(GET_METHOD, url, null).then(() => {
          selector.remove();
        });
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
  });
});