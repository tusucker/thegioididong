$(function () {
  $(document).on('click', '.delete-user', function (e) {
    e.preventDefault();
    let selector = $(this).parents('tr');
    let url = $(this).attr('href');
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        callApi(GET_METHOD, url, null).then(() => {
          selector.remove();
        });
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
  });
});