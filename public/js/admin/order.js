$(function () {
  $(document).on('click', '.show-detail', function (e) {
    e.preventDefault();
    let url = $(this).attr('href');
    callApi('get', url, null).then(function (rs) {
      $('#modal-detail').replaceWith(rs);
      
      $('#modal-detail').modal('show');
    });
  });
});