<?php
use Illuminate\Support\Facades\Route;

Route::prefix('frontend')->as('frontend.')->namespace('Frontend')->group(function () {
    Route::get('/',[
        'uses' => 'HomeController@index',
        'as' => 'index'
    ]);

    Route::post('login', [
        'uses' => 'HomeController@login',
        'as' => 'login'
    ]);

    Route::post('register', [
        'uses' => 'HomeController@register',
        'as' => 'register'
    ]);

    Route::get('logout', [
        'uses' => 'HomeController@logout',
        'as' => 'logout'
    ]);

    Route::get('show-categories/{slug}/{id}', [
        'uses' => 'HomeController@showCategory',
        'as' => 'show-categories'
    ]);

    Route::get('show-product-by-category-id/{id}/{parentId}', [
        'uses' => 'HomeController@showProduct',
        'as' => 'show-product'
    ]);

    Route::get('show-product-by-price/{parentId}', [
        'uses' => 'HomeController@showProductByPrice',
        'as' => 'show-product-by-price'
    ]);

    Route::get('show-detail-product/{id}/{parentId}', [
        'uses' => 'HomeController@showDetailProduct',
        'as' => 'show-detail-product'
    ]);

    Route::get('show-cart-detail', [
        'uses' => 'ShoppingCartController@showCartDetail',
        'as' => 'show-cart-detail'
    ]);

    Route::get('add-to-cart', [
        'uses' => 'ShoppingCartController@addToCart',
        'as' => 'add-to-cart'
    ]);

    Route::get('add-to-cart-simple/{productId}', [
        'uses' => 'ShoppingCartController@addToCartSimple',
        'as' => 'add-to-cart-simple'
    ]);

    Route::get('remove-product-from-cart/{rowId}', [
        'uses' => 'ShoppingCartController@removeProductFromCart',
        'as' => 'remove-product-from-cart'
    ]);

    Route::get('update-quantity-product-from-cart/{rowId}', [
        'uses' => 'ShoppingCartController@updateQuantityProductFromCart',
        'as' => 'update-quantity-product-from-cart'
    ]);

    Route::post('checkout', [
        'uses' => 'ShoppingCartController@checkout',
        'as' => 'checkout'
    ]);

    Route::get('list-article', [
        'uses' => 'ArticleController@listArticle',
        'as' => 'list-article'
    ]);

    Route::get('detail-article/{id}', [
        'uses' => 'ArticleController@showDetailArticle',
        'as' => 'detail-article'
    ]);

    Route::get('send-comment-article/{id}/{parentId}', [
        'uses' => 'ArticleController@sendComment',
        'as' => 'send-comment',
    ]);

    Route::get('send-comment-product/{id}/{parentId}', [
        'uses' => 'HomeController@sendComment',
        'as' => 'send-comment-product',
    ]);

    Route::get('send-comment-product-non-rating/{id}/{parentId}', [
        'uses' => 'HomeController@sendCommentNonRating',
        'as' => 'send-comment-product-non-rating',
    ]);

    Route::get('search', [
        'uses' => 'HomeController@search',
        'as' => 'search',
    ]);

    Route::get('add-to-wishlist/{id}', [
        'uses' => 'HomeController@addToWishList',
        'as' => 'add-to-wishlist',
    ]);

    Route::get('list-wishlist/', [
        'uses' => 'HomeController@listWishList',
        'as' => 'list-wishlist',
    ]);

    Route::get('remove-from-wishlist/{id}', [
        'uses' => 'HomeController@removeFromWishList',
        'as' => 'remove-from-wishlist',
    ]);

    Route::post('change-information',[
        'uses' => 'HomeController@changeInformation',
        'as' => 'change-information'
    ]);

    Route::post('change-password',[
        'uses' => 'HomeController@changePassword',
        'as' => 'change-password'
    ]);

    Route::get('show-detail-order/{id}', [
        'uses' => 'HomeController@showDetailOrder',
        'as' => 'show-detail-order'
    ]);

    Route::get('cancel-order/{id}', [
        'uses' => 'HomeController@cancelOrder',
        'as' => 'cancel-order'
    ]);
});