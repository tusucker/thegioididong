<?php
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->namespace('Admin')->group(function (){

    // login
    Route::get('/login', [
        'as' => 'admin.login',
        'uses' => 'AdminController@login'
    ]);
    Route::post('/login', [
        'as' => 'admin.login',
        'uses' => 'AdminController@post_login'
    ]);

    // Dashboard
    Route::get('dashboard',[
        'uses' => 'MemberController@showDashboard',
        'as' => 'admin.dashboard',
        'middleware' => 'can:view-dashboard'
    ]);

    // Category
    Route::prefix('categories')->group(function (){
        Route::get('/',[
            'uses' => 'CategoryController@index',
            'as' => 'categories.index',
            'middleware' => 'can:view-category'
        ]);
        Route::get('list-category',[
            'uses' => 'CategoryController@listCategory',
            'as' => 'categories.list'
        ]);
        Route::get('list-parent',[
            'uses' => 'CategoryController@listParent',
            'as' => 'categories.list-parent'
        ]);
        Route::post('store',[
            'uses' => 'CategoryController@store',
            'as' => 'categories.store',
            'middleware' => 'can:add-category'
        ]);
        Route::post('update/{id}',[
            'uses' => 'CategoryController@update',
            'as' => 'categories.update',
            'middleware' => 'can:edit-category'
        ]);
        Route::get('get-category/{id}',[
            'uses' => 'CategoryController@getCategory',
            'as' => 'categories.get-category'
        ]);
        Route::get('destroy/{id}',[
            'uses' => 'CategoryController@destroy',
            'as' => 'categories.destroy',
            'middleware' => 'can:delete-category'
        ]);
        Route::get('change-status/{id}',[
            'uses' => 'CategoryController@changeStatus',
            'as' => 'categories.change-status',
            'middleware' => 'can:edit-category'
        ]);
        Route::get('tree-view',[
            'uses'=>'CategoryController@treeView',
            'as'=>'categories.tree-view'
        ]);
    });

    // Product
    Route::prefix('products')->group(function(){
        Route::get('/',[
            'uses' => 'ProductController@index',
            'as' => 'products.index',
            'middleware' => 'can:view-product'
        ]);
        Route::get('list-product',[
            'uses'=>'ProductController@listProduct',
            'as'=>'products.list'
        ]);
        Route::get('change-status/{id}',[
            'uses'=>'ProductController@changeStatus',
            'as'=>'products.change-status',
            'middleware' => 'can:edit-product'
        ]);
        Route::post('store',[
            'uses'=>'ProductController@store',
            'as'=>'products.store',
            'middleware' => 'can:add-product'
        ]);
        Route::post('update/{id}',[
            'uses'=>'ProductController@update',
            'as'=>'products.update',
            'middleware' => 'can:edit-product'
        ]);
        Route::get('edit/{id}',[
            'uses'=>'ProductController@edit',
            'as'=>'products.edit',
            'middleware' => 'can:edit-product'
        ]);
        Route::get('destroy/{id}',[
            'uses'=>'ProductController@destroy',
            'as'=>'products.destroy',
            'middleware' => 'can:delete-product'
        ]);
    });

    // Article
    Route::prefix('articles')->group(function () {
        Route::get('/',[
            'uses' => 'ArticleController@index',
            'as' => 'articles.index',
            'middleware' => 'can:view-article'
        ]);
        Route::get('/create',[
            'uses' => 'ArticleController@create',
            'as' => 'articles.create',
            'middleware' => 'can:add-article'
        ]);
        Route::post('/store',[
            'uses' => 'ArticleController@store',
            'as' => 'articles.store',
            'middleware' => 'can:add-article'
        ]);
        Route::get('/{id}/edit', [
            'uses' => 'ArticleController@edit',
            'as' => 'articles.edit',
            'middleware' => 'can:edit-article'
        ]);
        Route::post('/{id}/update', [
            'uses' => 'ArticleController@update',
            'as' => 'articles.update',
            'middleware' => 'can:edit-article'
        ]);
        Route::get('/{id}/destroy',[
            'uses' => 'ArticleController@destroy',
            'as' => 'articles.destroy',
            'middleware' => 'can:delete-article'
        ]);
    });

    // users
    Route::prefix('users')->group(function () {
        Route::get('/', [
            'uses' => 'UserController@index',
            'as' => 'users.index',
            'middleware' => 'can:view-user'
        ]);

        Route::get('/create',[
            'uses' => 'UserController@create',
            'as' => 'users.create',
            'middleware' => 'can:add-user'
        ]);
        Route::post('/store',[
            'uses' => 'UserController@store',
            'as' => 'users.store',
            'middleware' => 'can:add-user'
        ]);
        Route::get('/{id}/edit', [
            'uses' => 'UserController@edit',
            'as' => 'users.edit',
            'middleware' => 'can:edit-user'
        ]);
        Route::post('/{id}/update', [
            'uses' => 'UserController@update',
            'as' => 'users.update',
            'middleware' => 'can:edit-user'
        ]);
        Route::get('/{id}/destroy',[
            'uses' => 'UserController@destroy',
            'as' => 'users.destroy',
            'middleware' => 'can:delete-user'
        ]);
    });

    // roles
    Route::prefix('roles')->group(function () {
        Route::get('/', [
            'uses' => 'RoleController@index',
            'as' => 'roles.index',
            'middleware' => 'can:view-role'
        ]);

        Route::get('/create',[
            'uses' => 'RoleController@create',
            'as' => 'roles.create',
            'middleware' => 'can:add-role'
        ]);
        Route::post('/store',[
            'uses' => 'RoleController@store',
            'as' => 'roles.store',
            'middleware' => 'can:add-role'
        ]);
        Route::get('/{id}/edit', [
            'uses' => 'RoleController@edit',
            'as' => 'roles.edit',
            'middleware' => 'can:edit-role'
        ]);
        Route::post('/{id}/update', [
            'uses' => 'RoleController@update',
            'as' => 'roles.update',
            'middleware' => 'can:edit-role'
        ]);
        Route::get('/{id}/destroy',[
            'uses' => 'RoleController@destroy',
            'as' => 'roles.destroy',
            'middleware' => 'can:delete-role'
        ]);
    });

    Route::prefix('orders')->group(function () {
        Route::get('/', [
            'uses' => 'OrderController@index',
            'as' => 'orders.index'
        ]);

        Route::get('detail/{id}', [
            'uses' => 'OrderController@detail',
            'as' => 'orders.detail'
        ]);

        Route::get('status-to-transport/{id}', [
            'uses' => 'OrderController@changeStatusToTransport',
            'as' => 'orders.status-to-transport'
        ]);

        Route::get('status-to-done/{id}', [
            'uses' => 'OrderController@changeStatusToDone',
            'as' => 'orders.status-to-done'
        ]);
    });
});